var webpack = require("webpack");
var common = require("./webpack.config.common");
var CopyWebpackPlugin = require('copy-webpack-plugin');

console.log("Bundling for production...");

module.exports = {
  entry: common.config.entry,
  output: {
    filename: '[name].js',
    path: common.config.publicDir,
  },
  module: {
    rules: common.getModuleRules()
  },
  plugins: common.getPlugins().concat([
    // new ExtractTextPlugin('style.css'),
    new CopyWebpackPlugin([ { from: common.config.templatesFolder } ])
  ]),
  resolve: {
    modules: [common.config.nodeModulesDir]
  },
};
