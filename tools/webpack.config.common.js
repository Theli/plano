var fs = require("fs");
var path = require("path");
var fableUtils = require("fable-utils");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackPolyfillIOPlugin = require('html-webpack-polyfill-io-plugin');
// var DynamicCdnWebpackPlugin = require('dynamic-cdn-webpack-plugin');

const projectFolder = resolve('../src/Client');
const projectFile = fs.readdirSync(projectFolder).find(f => f.endsWith('.fsproj'));
const entry = path.join(projectFolder, projectFile);
const templatesFolder = path.join(projectFolder, 'Templates');
const indexHtmlTemplate = path.join(templatesFolder, 'index.html');



var config = {
  entry: entry,
  publicDir: resolve("../public"),
  nodeModulesDir: resolve("../node_modules"),
  templatesFolder: templatesFolder,
  indexHtmlTemplate: indexHtmlTemplate
}

function resolve(filePath) {
  return path.join(__dirname, filePath)
}


function getModuleRules(isProduction) {
  var babelOptions = fableUtils.resolveBabelOptions({
    presets: [
      ["env", { "targets": { "browsers": "> 1%" }, "modules": false }]
    ],
  });

  return [
    {
      test: /\.fs(x|proj)?$/,
      use: {
        loader: "fable-loader",
        options: {
          babel: babelOptions,
          define: isProduction ? [] : ["DEBUG"]
        }
      }
    },
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: babelOptions
      },
    }
  ];
}

function getPlugins(isProduction) {
  return [
    new HtmlWebpackPlugin({
      filename: path.join(config.publicDir, "index.html"),
      template: config.indexHtmlTemplate,
      // minify: isProduction ? {} : false
    }),
    new HtmlWebpackPolyfillIOPlugin({ features: "es6,fetch" }),
    // new DynamicCdnWebpackPlugin({ verbose: true, only: config.cdnModules }),
  ];
}

module.exports = {
  resolve: resolve,
  config: config,
  getModuleRules: getModuleRules,
  getPlugins: getPlugins
}
