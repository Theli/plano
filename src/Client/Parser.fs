module Planograms.Parser
//Approach and data is taken From PSALib by Phill Dayton
open Model
open System
open SharedExtensions.Math
open Fable.AST.Babel
open Geometry

[<Literal>]
let UseUpperLevelSettings = -1

type ProductPSAColumns = 
    | UPC = 1
    | ID = 2
    | Name = 3
    | Width = 5
    | Height = 6
    | Depth = 7
    | Color = 8
    | Manufacturer = 12
    | Category = 13
    | Supplier = 14
    | Price = 33
    | Brand = 232
    | SubCategory = 233
    | SqueezeLimitWidth = 224
    | SqueezeLimitHeight = 225
    | SqueezeLimitDepth = 226
    | ExpandLimitWidth = 227
    | ExpandLimitHeight = 228
    | ExpandLimitDepth = 229
    | NestingWidth = 16
    | NestingHeight = 17
    | NestingDepth = 18
    | NumberOfPegHoles = 19
    | PegHole1XPositionFromLeft = 20
    | PegHole1YPositionFromTop = 21
    | PegHole2XPositionFromLeft = 23
    | PegHole2YPositionFromTop = 24
    | PegHole3XPositionFromLeft = 26
    | PegHole3YPositionFromTop = 27
    | YMinimumUnitsHigh = 95
    | YMaximumUnitsHigh = 96
    | TargetUnitsHigh = 97
    | TargetCapsNestsHigh = 98 


type PositionPSAColumns = 
    | UPC = 1
    | ID = 2
    | X = 4
    | Y = 6
    | Z = 8
    | XCount = 14
    | YCount = 15
    | ZCount = 16
    | Orientation = 29
    | XCappingCount = 17
    | XCappingAlignment = 19
    | XCappingOrientation = 20
    | YCappingCount = 21
    | YCappingAlignment = 23
    | YCappingOrientation = 24
    | ZCappingCount = 25
    | ZCappingAlignment = 27
    | ZCappingOrientation = 28
    | YMinimumUnitsHigh = 60
    | YMaximumUnitsHigh = 61
    | TargetUnitsHigh = 62
    | TargetCapsNestsHigh = 63 
    | MerchStyle = 13


    

type MerchFacingsCount = 
    | DefaultInherited = 0
    | ManuallyDetermined = 1
    | OnlyOne = 2
    | FillAvailableSpace = 3

type MerchFacingsMode = 
    | DefaultInherited = 0
    | Normal = 1
    | ExpandOrSqueeze = 2
    | SpacedOut = 3

  type MerchPlacement = 
    | DefaultInherited = 0
    | ManuallyPlaced = 1
    | FurthestExtremity = 2
    | StackedAgainstOthers = 3
    | SpreadEvenly = 4

  type MerchXDirection = 
    | DefaultInherited = -1
    | LeftToRight = 0
    | RightToLeft = 1

type PSAFixtureType = 
    | Shelf = 0
    // | Chest = 1
    // | Bin = 2
    // | Polygonal_Shelf = 3
    // | Rod = 4
    // | LateralRod = 5
    | Bar = 6
    | Pegboard = 7
    // | MultiRowPegboard = 8
    // | CurvedRod = 9
    // | Obstruction = 10
    // | Sign = 11
    // | GravityFeed = 12
 
type FixturePSAColumns = 
    | Type = 1
    | Name = 2
    | X = 4
    | Y = 6
    | Z = 8
    | Width = 5
    | Height = 7
    | Depth = 9
    | Color = 13
    | MerchFacingsCount = 63 // isAutofill = MerchFacingsCount.FillAvailableSpace
    | WidthMerchFacingsMode = 55 // squeeze expand if = MerchFacingsMode.ExpandOrSqueeze
    | HeightMerchFacingsMode = 64 // squeeze expand if = MerchFacingsMode.ExpandOrSqueeze
    | DepthMerchFacingsMode = 73 // squeeze expand if = MerchFacingsMode.ExpandOrSqueeze
    | XPlacementDirection = 56 // MerchXDirection
    | XPlacementLeftToRightSetting = 53 // MerchPlacement
    | YMinimumUnitsHigh = 58
    | YMaximumUnitsHigh = 59
    | TargetUnitsHigh = 60
    | TargetCapsNestsHigh = 61     
    // | DetectOtherFixtures = 24
    // | DetectPositionsOnOtherFixtures = 25
    | CanCombine = 37

type SpikeFixturePSAColumns = 
    | Name = 2
    | X = 4
    | Y = 6
    | Z = 8
    | Width = 5
    | Height = 7
    | Depth = 9
    | Color = 13
    | HorizontalSpacing = 15
    | VerticalSpacing = 16
    | StartingHorizontal = 17
    | StartingVertical = 18

type PlanogramPSAColumns = 
    | Name = 1
    | Width = 3
    | Height = 4
    | Depth = 5
    | YMinimumUnitsHigh = 35
    | YMaximumUnitsHigh = 36
    | TargetUnitsHigh = 37
    | TargetCapsNestsHigh = 38        

type PlanogramProjectPSAColumns = 
    | Name = 1
    | UnitOfMeasurement = 12
    | YMinimumUnitsHigh = 23
    | YMaximumUnitsHigh = 24
    | TargetUnitsHigh = 25
    | TargetCapsNestsHigh = 26         

type Vector3PsaUnits = Vector3PsaUnits of float * float * float
type Vector2PsaUnits = Vector2PsaUnits of float * float

let psaVector3ToMetricVector unitsToMetric = function
    | Vector3PsaUnits (x, y, z) -> {X = (unitsToMetric x); Y = (unitsToMetric y); Z = (unitsToMetric z); }

let psaVector2ToMetricVector unitsToMetric = function
    | Vector2PsaUnits (x, y) -> {X = (unitsToMetric x); Y = (unitsToMetric y) }    
let psaColorToRGB psaColor =
    let channel i = 
        (psaColor >>> (i * 8)) &&& 0xFF
    let r = channel 2            
    let g = channel 1     
    let b = channel 0

    r + (g<<<8) + (b<<<16)

type EnumArray<'e when 'e : enum<int>>(columns: string[]) = 
    member this.Item 
        with get (idx: 'e) = 
            Array.item (unbox<int> idx) columns
            |> fun s -> if s |> System.String.IsNullOrEmpty then None else s |> Some 
    member this.GetString idx = 
        this.[idx] |> Option.defaultValue String.Empty

    member this.GetFloat idx = 
        this.[idx]
        |> Option.bind (fun c ->
            let ok, f = System.Double.TryParse c
            if ok then Some f else None)
        |> Option.defaultValue 0.        
    member this.TryGetInt idx = 
        this.[idx]
        |> Option.bind (fun c ->
            let ok, i = System.Int32.TryParse c
            if ok then Some i else None)
      member this.GetInt idx = 
        idx
        |> this.TryGetInt 
        |> Option.defaultValue 0      

    member this.TryGetNotDefaultInt idx = 
        this.[idx]
        |> Option.bind (fun c ->
            let ok, i = System.Int32.TryParse c
            if ok && i <> UseUpperLevelSettings then Some i else None)
    member this.GetEnum<'t when 't: enum<int>> (idx) : 't = 
        idx |> this.GetInt |> LanguagePrimitives.EnumOfValue

    member this.GetVector3PsaUnits (xCol, yCol, zCol) = 
        Vector3PsaUnits ((this.GetFloat xCol), (this.GetFloat yCol), (this.GetFloat zCol))

    member this.GetVector2PsaUnits (xCol, yCol) = 
        Vector2PsaUnits ((this.GetFloat xCol), (this.GetFloat yCol))
let minNestingSize = 
    let min = 0.0001
    {X = min; Y = min; Z = min}
let minSqueezeFactor = 
    let min = 0.01
    {X = min; Y = min; Z = min}

let maxSqueezeFactor = 
    let max = 1.
    {X = max; Y = max; Z = max}

let minExpandFactor = 
    let min = 1.
    {X = min; Y = min; Z = min}

let maxExpandFactor = 
    let max = 999.99    
    {X = max; Y = max; Z = max}    

let clamp min max v = 
    let x = v.X |> clamp min.X max.X
    let y = v.Y |> clamp min.Y max.Y
    let z = v.Z |> clamp min.Z max.Z
    { X = x; Y = y; Z = z }

let parseProduct unitsToMetric (columns: EnumArray<ProductPSAColumns>) = 
    let upc = columns.GetString ProductPSAColumns.UPC
    let size = 
        (ProductPSAColumns.Width, ProductPSAColumns.Height, ProductPSAColumns.Depth)
        |> columns.GetVector3PsaUnits 
        |> psaVector3ToMetricVector unitsToMetric

    let nestingSize = 
        (ProductPSAColumns.NestingWidth, ProductPSAColumns.NestingHeight, ProductPSAColumns.NestingDepth)
        |> columns.GetVector3PsaUnits 
        |> psaVector3ToMetricVector unitsToMetric
        |> fun s -> clamp minNestingSize s s

    let squeezeLimit = 
        (ProductPSAColumns.SqueezeLimitWidth, ProductPSAColumns.SqueezeLimitHeight, ProductPSAColumns.SqueezeLimitDepth)
        |> columns.GetVector3PsaUnits 
        |> psaVector3ToMetricVector id
        |> clamp minSqueezeFactor maxSqueezeFactor

    let expandLimit = 
        (ProductPSAColumns.ExpandLimitWidth, ProductPSAColumns.ExpandLimitHeight, ProductPSAColumns.ExpandLimitDepth)
        |> columns.GetVector3PsaUnits 
        |> psaVector3ToMetricVector id
        |> clamp minExpandFactor maxExpandFactor

    let pegholeCount = ProductPSAColumns.NumberOfPegHoles |> columns.GetInt

    let mainSpike = 
        if pegholeCount = 0 then 
            { X = size.X / 2.; Y = 0.} 
        else 
            (ProductPSAColumns.PegHole1XPositionFromLeft, ProductPSAColumns.PegHole1YPositionFromTop) 
            |> columns.GetVector2PsaUnits
            |> psaVector2ToMetricVector id

    let secondarySpikes = 
        match pegholeCount with
        | 2 -> [(ProductPSAColumns.PegHole2XPositionFromLeft, ProductPSAColumns.PegHole2YPositionFromTop) ]        
        | 3 -> [(ProductPSAColumns.PegHole2XPositionFromLeft, ProductPSAColumns.PegHole2YPositionFromTop); (ProductPSAColumns.PegHole3XPositionFromLeft, ProductPSAColumns.PegHole3YPositionFromTop) ]   
        | _ -> []
        |> List.map (columns.GetVector2PsaUnits >> (psaVector2ToMetricVector id))     

    let color = 
        ProductPSAColumns.Color 
        |> columns.TryGetInt 
        |> Option.map psaColorToRGB 
        |> Option.defaultValue 0x808080 

    let merchProperties = 
        [
            columns.[ProductPSAColumns.Name] |> Option.map ProductInfo.ProductName
            columns.[ProductPSAColumns.Manufacturer] |> Option.map ProductInfo.Manufacturer
            columns.[ProductPSAColumns.Category] |> Option.map ProductInfo.Category
            columns.[ProductPSAColumns.Supplier] |> Option.map ProductInfo.Supplier 
            columns.[ProductPSAColumns.Price] |> Option.map (float >> ProductInfo.Price)
            columns.[ProductPSAColumns.Brand] |> Option.map ProductInfo.Brand
            columns.[ProductPSAColumns.SubCategory] |> Option.map ProductInfo.SubCategory 
        ]
        |> List.choose id     
        |> Set.ofList          

    let yAutofill = 
        {
            MaximumTotal = columns.TryGetNotDefaultInt ProductPSAColumns.YMaximumUnitsHigh  
            MinimumTotal = columns.TryGetNotDefaultInt ProductPSAColumns.YMinimumUnitsHigh  
            MaximumUnits = columns.TryGetNotDefaultInt ProductPSAColumns.TargetUnitsHigh 
            MaximumCapping = columns.TryGetNotDefaultInt ProductPSAColumns.TargetCapsNestsHigh 
        }               
    {
        Upc = upc  
        Size = size
        NestingSize = nestingSize
        SqueezeLimit = squeezeLimit
        ExpandLimit = expandLimit
        Color = color 
        ProductInfo = merchProperties     
        MainSpikeOffset = mainSpike
        SecondarySpikeOffsets = secondarySpikes
        AutofillOptions = yAutofill
    }

let (|Product|_|) unitsToMetric (line: string) = 
    if line.StartsWith("Product,") then
        line |> String.splitByChar ',' |> EnumArray<ProductPSAColumns> |> parseProduct unitsToMetric |> Some
    else None
let parseOrientation = function
    | "0" -> {Side = Front; Roll = Deg0}
    | "1" -> {Side = Front; Roll = Deg90}
    | "12" -> {Side = Front; Roll = Deg180}
    | "13" -> {Side = Front; Roll = Deg270}
    | "6" -> {Side = Back; Roll = Deg0}
    | "7" -> {Side = Back; Roll = Deg90}
    | "18" -> {Side = Back; Roll = Deg180}
    | "19" -> {Side = Back; Roll = Deg270}
    | "2" -> {Side = Left; Roll = Deg0}
    | "3" -> {Side = Left; Roll = Deg90}
    | "14" -> {Side = Left; Roll = Deg180}
    | "15" -> {Side = Left; Roll = Deg270}
    | "8" -> {Side = Right; Roll = Deg0}
    | "9" -> {Side = Right; Roll = Deg90}
    | "20" -> {Side = Right; Roll = Deg180}
    | "21" -> {Side = Right; Roll = Deg270}
    | "4" -> {Side = Top; Roll = Deg0}
    | "5" -> {Side = Top; Roll = Deg90}
    | "16" -> {Side = Top; Roll = Deg180}
    | "17" -> {Side = Top; Roll = Deg270}
    | "10" -> {Side = Bottom; Roll = Deg0}
    | "11" -> {Side = Bottom;Roll = Deg90}
    | "22" -> {Side = Bottom; Roll = Deg180}
    | "23" -> {Side = Bottom; Roll = Deg270}
    | _ -> {Side = Front; Roll = Deg0}

let parsePosition unitsToMetric fixtureAutofillSettings upcToProductMap (columns: EnumArray<PositionPSAColumns>)= 
    let xCount = PositionPSAColumns.XCount |> columns.GetInt
    let yCount = PositionPSAColumns.YCount |> columns.GetInt
    let zCount = PositionPSAColumns.ZCount |> columns.GetInt

    let upc = columns.GetString PositionPSAColumns.UPC
    let product = upcToProductMap |> Map.find upc
    let position = 
        (PositionPSAColumns.X, PositionPSAColumns.Y, PositionPSAColumns.Z) 
        |> columns.GetVector3PsaUnits 
        |> psaVector3ToMetricVector unitsToMetric
    let orientation = 
        PositionPSAColumns.Orientation
        |> columns.GetString 
        |> parseOrientation
    let xCapping = 
        let count = PositionPSAColumns.XCappingCount |> columns.GetInt
        if count = 0 then None
        else
            let orientation = 
                PositionPSAColumns.XCappingOrientation
                |> columns.GetString 
                |> parseOrientation
            PositionPSAColumns.XCappingAlignment 
            |> columns.GetInt 
            |> function 
            | 1 ->
               // printfn "is left"  todo in docs is left = 0, but in vri result it is reverted. 
                (LeftSide, orientation, count) 
            | _ -> (RightSide, orientation, count)
            |> Some        
    let yCapping = 
        let count = PositionPSAColumns.YCappingCount |> columns.GetInt
        if count = 0 then None
        else
            let orientation = 
                PositionPSAColumns.YCappingOrientation
                |> columns.GetString 
                |> parseOrientation
            PositionPSAColumns.YCappingAlignment 
            |> columns.GetInt 
            |> function | 0 -> (Above, orientation, count) | _ -> (Below, orientation, count)
            |> Some
    let zCapping = 
        let count = PositionPSAColumns.ZCappingCount |> columns.GetInt
        if count = 0 then None
        else
            let orientation = 
                PositionPSAColumns.ZCappingOrientation
                |> columns.GetString 
                |> parseOrientation
            PositionPSAColumns.ZCappingAlignment 
            |> columns.GetInt 
            |> function | 0 -> (InFront, orientation, count) | _ -> (Behind, orientation, count)
            |> Some     

    let defaultMaxTotal = 99            
    let defaultMinTotal = 1            
    let defaultMaxUnits = 99            
    let defaultMaxCappings = 99

    let yAutofillMaxTotal = 
        columns.TryGetNotDefaultInt PositionPSAColumns.YMaximumUnitsHigh
        |> Option.orElse product.AutofillOptions.MaximumTotal 
        |> Option.orElse fixtureAutofillSettings.MaximumTotal
        |> Option.filter (fun x -> x > 0 && x <= defaultMaxTotal)
        |> Option.defaultValue defaultMaxTotal

    let yAutofillMinTotal = 
        columns.TryGetNotDefaultInt PositionPSAColumns.YMinimumUnitsHigh
        |> Option.orElse product.AutofillOptions.MinimumTotal 
        |> Option.orElse fixtureAutofillSettings.MinimumTotal
        |> Option.filter (fun x -> x > 0 && x < yAutofillMaxTotal)
        |> Option.defaultValue defaultMinTotal

    let yAutofillMaxUnits = 
        columns.TryGetNotDefaultInt PositionPSAColumns.TargetUnitsHigh
        |> Option.orElse product.AutofillOptions.MaximumUnits 
        |> Option.orElse fixtureAutofillSettings.MaximumUnits
        |> Option.filter (fun x -> x <= defaultMaxUnits)
        |> Option.defaultValue defaultMaxUnits

    let yAutofillMaxCappings = 
        columns.TryGetNotDefaultInt PositionPSAColumns.TargetCapsNestsHigh
        |> Option.orElse product.AutofillOptions.MaximumCapping 
        |> Option.orElse fixtureAutofillSettings.MaximumCapping
        |> Option.filter (fun x -> x <= defaultMaxCappings)
        |> Option.defaultValue defaultMaxCappings

    let yAutofill = 
        {
            MaximumTotal = yAutofillMaxTotal
            MinimumTotal = yAutofillMinTotal
            MaximumUnits = yAutofillMaxUnits
            MaximumCapping = yAutofillMaxCappings
            CappingOrientation = PositionPSAColumns.YCappingOrientation
                |> columns.GetString 
                |> parseOrientation
        }

    let merchStyle = 
        PositionPSAColumns.MerchStyle
        |> columns.GetInt
        |> fun x ->
            match (enum x) with
            | MerchStyle.Case -> MerchStyle.Case
            | MerchStyle.Tray -> MerchStyle.Tray
            | _ -> MerchStyle.Unit

    let pg = {
        Product = product
        Orientation = orientation
        Count = xCount, yCount, zCount
        XCapping = xCapping
        YCapping = yCapping
        ZCapping = zCapping
        YAutofill = yAutofill
        MerchStyle = merchStyle
    }
    pg, position

let (|Position|_|) unitsToMetric fixtureAutofillSettings upcToProductMap (line: string) = 
    if line.StartsWith("Position,") then
        line |> String.splitByChar ',' |> EnumArray<PositionPSAColumns> |> parsePosition unitsToMetric fixtureAutofillSettings upcToProductMap |> Some
    else None

let parseShelfProperties unitsToMetric variantAutofillSettings (columns: EnumArray<FixturePSAColumns>) = 
    let name = columns.GetString FixturePSAColumns.Name
    let size = 
        (FixturePSAColumns.Width, FixturePSAColumns.Height, FixturePSAColumns.Depth)
        |> columns.GetVector3PsaUnits
        |> psaVector3ToMetricVector unitsToMetric
    let position = 
        (FixturePSAColumns.X, FixturePSAColumns.Y, FixturePSAColumns.Z)
        |> columns.GetVector3PsaUnits
        |> psaVector3ToMetricVector unitsToMetric
    let color = 
        FixturePSAColumns.Color 
        |> columns.TryGetInt 
        |> Option.map psaColorToRGB 
        |> Option.defaultValue 0x808080 

    let merchFacingsCount = 
        FixturePSAColumns.MerchFacingsCount
        |> columns.GetEnum<MerchFacingsCount> 

    let isYAutofillEnabled = 
        merchFacingsCount
        |> ((=) MerchFacingsCount.FillAvailableSpace)

    let xMerchFacingsMode = 
        FixturePSAColumns.WidthMerchFacingsMode
        |> columns.GetEnum<MerchFacingsMode> 

    let xSqueezeEnabled = 
        xMerchFacingsMode
        |> ((=) MerchFacingsMode.ExpandOrSqueeze)

    let ySqueezeEnabled = 
        FixturePSAColumns.HeightMerchFacingsMode
        |> columns.GetEnum<MerchFacingsMode> 
        |> ((=) MerchFacingsMode.ExpandOrSqueeze)

    let zSqueezeEnabled = 
        FixturePSAColumns.DepthMerchFacingsMode
        |> columns.GetEnum<MerchFacingsMode> 
        |> ((=) MerchFacingsMode.ExpandOrSqueeze)
    
    let xPlacement = 
         FixturePSAColumns.XPlacementLeftToRightSetting
        |> columns.GetEnum<MerchPlacement>    

    let xDirection = 
         FixturePSAColumns.XPlacementDirection
        |> columns.GetEnum<MerchXDirection>   

    let layout = 
        match xPlacement with
        | MerchPlacement.ManuallyPlaced -> None
        | _ when xMerchFacingsMode = MerchFacingsMode.SpacedOut -> Some Spaced
        | MerchPlacement.SpreadEvenly -> Some Spread
        | MerchPlacement.StackedAgainstOthers when xDirection = MerchXDirection.LeftToRight -> Some SnapLeft
        | MerchPlacement.StackedAgainstOthers -> Some SnapRight
        | MerchPlacement.FurthestExtremity -> Some Spread
        | _ -> None

    let yAutofill = 
        {
            MaximumTotal = (columns.TryGetNotDefaultInt FixturePSAColumns.YMaximumUnitsHigh) |> Option.orElse variantAutofillSettings.MaximumTotal  
            MinimumTotal = (columns.TryGetNotDefaultInt FixturePSAColumns.YMinimumUnitsHigh) |> Option.orElse variantAutofillSettings.MinimumTotal   
            MaximumUnits = (columns.TryGetNotDefaultInt FixturePSAColumns.TargetUnitsHigh) |> Option.orElse variantAutofillSettings.MaximumUnits  
            MaximumCapping = (columns.TryGetNotDefaultInt FixturePSAColumns.TargetCapsNestsHigh) |> Option.orElse variantAutofillSettings.MaximumCapping  
        }        

    let canCombine = FixturePSAColumns.CanCombine |> columns.GetInt |> ((<>) 0)  //todo i'm not sure about this psa column. ask somebody about it


    name, size, position, layout, color, isYAutofillEnabled, (xSqueezeEnabled, ySqueezeEnabled, zSqueezeEnabled), yAutofill, canCombine

let parseSpikeFixtureProperties unitsToMetric (columns: EnumArray<SpikeFixturePSAColumns>) = 
    let name = columns.GetString SpikeFixturePSAColumns.Name
    let DEFAULT_Y_SPACING = 0.0254
    let size = 
        (SpikeFixturePSAColumns.Width, SpikeFixturePSAColumns.Height, SpikeFixturePSAColumns.Depth)
        |> columns.GetVector3PsaUnits
        |> psaVector3ToMetricVector unitsToMetric
    let position = 
        (SpikeFixturePSAColumns.X, SpikeFixturePSAColumns.Y, SpikeFixturePSAColumns.Z)
        |> columns.GetVector3PsaUnits
        |> psaVector3ToMetricVector unitsToMetric
    let color = 
        SpikeFixturePSAColumns.Color 
        |> columns.TryGetInt 
        |> Option.map psaColorToRGB 
        |> Option.defaultValue 0x408080 
    let xSpacing = SpikeFixturePSAColumns.HorizontalSpacing |> columns.GetFloat |> unitsToMetric

    let ySpacing = 
        match SpikeFixturePSAColumns.VerticalSpacing |> columns.GetFloat with
        | rawYSpacing when rawYSpacing |> nearEquals 0. && xSpacing |> nearEquals 0. |> not -> xSpacing
        | rawYSpacing when rawYSpacing |> nearEquals 0. -> DEFAULT_Y_SPACING
        | rawYSpacing -> rawYSpacing |> unitsToMetric

    let xOffset = 
        match SpikeFixturePSAColumns.StartingHorizontal |> columns.GetFloat with
        | x when x |> nearEquals 0. -> xSpacing
        | x -> x |> unitsToMetric

    let yOffset = 
        match SpikeFixturePSAColumns.StartingVertical |> columns.GetFloat with
        | y when y |> nearEquals 0. -> ySpacing
        | y -> y |> unitsToMetric     

    let offset = {X = xOffset; Y = yOffset}
    let spacing = {X = xSpacing; Y = ySpacing}

    name, size, position, color, spacing, offset

let parsePlanogram unitsToMetric projectAutofillSettings (columns:EnumArray<PlanogramPSAColumns>) = 
    let name = columns.GetString PlanogramPSAColumns.Name
    let size = 
        (PlanogramPSAColumns.Width, PlanogramPSAColumns.Height, PlanogramPSAColumns.Depth)
        |> columns.GetVector3PsaUnits
        |> psaVector3ToMetricVector unitsToMetric
    let yAutofill = 
        {
            MaximumTotal = (columns.TryGetNotDefaultInt PlanogramPSAColumns.YMaximumUnitsHigh) |> Option.orElse projectAutofillSettings.MaximumTotal    
            MinimumTotal = (columns.TryGetNotDefaultInt PlanogramPSAColumns.YMinimumUnitsHigh) |> Option.orElse projectAutofillSettings.MinimumTotal      
            MaximumUnits = (columns.TryGetNotDefaultInt PlanogramPSAColumns.TargetUnitsHigh) |> Option.orElse projectAutofillSettings.MaximumUnits     
            MaximumCapping = (columns.TryGetNotDefaultInt PlanogramPSAColumns.TargetCapsNestsHigh) |> Option.orElse projectAutofillSettings.MaximumCapping     
        }             
    name, size, yAutofill   
let parsePlanogramProject (columns: EnumArray<PlanogramProjectPSAColumns>) = 
    let name = columns.GetString PlanogramProjectPSAColumns.Name
    let units = columns.GetEnum<UnitsOfMeasurement> PlanogramProjectPSAColumns.UnitOfMeasurement
    let yAutofill = 
        {
            MaximumTotal = columns.TryGetNotDefaultInt PlanogramProjectPSAColumns.YMaximumUnitsHigh  
            MinimumTotal =  columns.TryGetNotDefaultInt PlanogramProjectPSAColumns.YMinimumUnitsHigh  
            MaximumUnits = columns.TryGetNotDefaultInt PlanogramProjectPSAColumns.TargetUnitsHigh 
            MaximumCapping = columns.TryGetNotDefaultInt PlanogramProjectPSAColumns.TargetCapsNestsHigh 
        }         
    name, units, yAutofill
    
open Planograms.Calculations.SpikeFixture
let parsePSA (text: string) =
    let anyFixturePrefix = "Fixture," 
    let shelfPrefix = sprintf "%s%i" anyFixturePrefix (PSAFixtureType.Shelf |> int)
    let pegboardPrefix = sprintf "%s%i" anyFixturePrefix (PSAFixtureType.Pegboard |> int)
    let barPrefix = sprintf "%s%i" anyFixturePrefix (PSAFixtureType.Bar |> int)
    
    let rec parseFixturePositions unitsToMetric fixtureAutofillSettings products positions lines = 
        match lines with
        | (Position unitsToMetric fixtureAutofillSettings products (productGroup, productPosition))::t -> 
            parseFixturePositions unitsToMetric fixtureAutofillSettings products ((productGroup, productPosition)::positions) t
        | _ -> positions, lines

    let rec parsePlanogramFixtures unitsToMetric variantAutofillSettings products fixtures (lines: string list) = 
        match lines with                      
        | h::t when h.StartsWith(shelfPrefix) ->
            let name, size, shelfPosition, autoLayout, color, isYAutofillEnabled, squeezeExpandEnabled, fixtureAutofillSettings, canCombine = 
                h |> String.splitByChar ',' |> EnumArray<FixturePSAColumns> |> parseShelfProperties unitsToMetric variantAutofillSettings
            let (positions, restLines) = 
                parseFixturePositions unitsToMetric fixtureAutofillSettings products [] t
                |> fun (positions, restLines) ->
                    let positionsWithRelativeX = 
                        positions
                        |> List.map (fun (pg, productPosition) -> pg, (productPosition - shelfPosition).X)
                    positionsWithRelativeX, restLines

            let productGroups = 
                match autoLayout with
                | None -> 
                    Manual positions   
                | Some layout ->
                    positions
                    |> List.sortBy (fun (_, x) -> x)
                    |> List.map first               
                    |> fun pgs -> Auto (layout, pgs)

            let shelf = { FixtureName = name; GeneratedId = random.Next(); Size = size; Color = color; ProductGroups = productGroups; IsAutofillEnabled = isYAutofillEnabled; SqueezeExpandEnabled = squeezeExpandEnabled; CanCombine = canCombine } 
            parsePlanogramFixtures unitsToMetric variantAutofillSettings products ((shelf, shelfPosition)::fixtures) restLines

        | h::t when h.StartsWith(pegboardPrefix) || h.StartsWith(barPrefix) ->
            let name, size, shelfPosition, color, spacing, offset = 
                h |> String.splitByChar ',' |> EnumArray<SpikeFixturePSAColumns> |> parseSpikeFixtureProperties unitsToMetric

            let (positions, restLines) = 
                parseFixturePositions unitsToMetric variantAutofillSettings products [] t
                |> fun (positions, restLines) ->
                    let positionsWithRelativeX = 
                        positions
                        |> List.map (fun (pg, productPosition) -> pg, (productPosition - shelfPosition))
                    positionsWithRelativeX, restLines

            let productGroups = 
                if h.StartsWith(pegboardPrefix) && spacing.X |> nearEquals 0. then //slatwall
                    positions
                    |> List.map (fun (productGroup, pgPos) -> 
                        let (mainSpike, lineNumber) = mainSpikePositionAndItsLine spacing offset size productGroup pgPos
                        let xOffset = max 0. (mainSpike.X - offset.X)
                        productGroup, (lineNumber, xOffset))
                    |> fun pgs -> SpikeFixture (SlatWall (spacing.Y, offset, pgs))
                elif h.StartsWith(pegboardPrefix) then //pegboard
                    positions
                    |> List.map (fun (productGroup, pgPos) -> 
                        let (mainSpike, lineNumber) = mainSpikePositionAndItsLine spacing offset size  productGroup pgPos                     
                        let xHoleIndex = (max (mainSpike.X - offset.X) 0.) |> divideBy spacing.X |> int                        
                        productGroup, (xHoleIndex, lineNumber))
                    |> fun pgs -> SpikeFixture (Pegboard (spacing, offset, pgs) )
                else //bar
                    let barSpacing = {X = 0.0001; Y = size.Y}
                    positions
                    |> List.map (fun (productGroup, pgPos) -> 
                        let (mainSpike, _) = mainSpikePositionAndItsLine barSpacing Vector2.Zero size productGroup pgPos
                        let xOffset = max 0. (mainSpike.X - offset.X)
                        productGroup, xOffset)
                    |> fun pgs -> SpikeFixture (Bar pgs)


            let shelf = { FixtureName = name; GeneratedId = random.Next(); Size = size; Color = color; ProductGroups = productGroups; IsAutofillEnabled = false; SqueezeExpandEnabled = (false, false, false); CanCombine = false }//todo canccombine
            parsePlanogramFixtures unitsToMetric variantAutofillSettings products ((shelf, shelfPosition)::fixtures) restLines

        | h::t when h.StartsWith(anyFixturePrefix) -> 
            let (_, restLines) = 
                parseFixturePositions unitsToMetric variantAutofillSettings products [] t           
            parsePlanogramFixtures unitsToMetric variantAutofillSettings products fixtures restLines

        | h::_ when h.StartsWith("Planogram,") -> fixtures, lines  
        | [] -> fixtures, []  
        | _::t  -> parsePlanogramFixtures unitsToMetric variantAutofillSettings products fixtures t

    let rec parse unitsToMetric projectAutofillSettings products planograms lines = 
        match lines, planograms with
        | (Product unitsToMetric p)::t, _ -> 
            let updatedProducts = products |> Map.add p.Upc p
            parse unitsToMetric projectAutofillSettings updatedProducts planograms t
        | h::t, existingPlanograms when h.StartsWith("Planogram,") ->
            let (planoName, planoSize, variantAutofillSettings) = h |> String.splitByChar ','|> EnumArray<PlanogramPSAColumns> |> parsePlanogram unitsToMetric projectAutofillSettings
            let (shelves, restLines) = parsePlanogramFixtures unitsToMetric variantAutofillSettings products [] t
            let plano = Planogram(planoName, planoSize, shelves)
            parse unitsToMetric projectAutofillSettings products (plano::existingPlanograms) restLines           
        | _::t, __ -> parse unitsToMetric projectAutofillSettings products planograms t
        | _ -> products, (planograms |> List.rev)

    let rec parseProject (lines: string list) = 
        match lines with   
        | h::t when h.StartsWith("Project,") ->       
            let projectName, units, yAutofill = h |> String.splitByChar ',' |> EnumArray<PlanogramProjectPSAColumns> |> parsePlanogramProject
            let unitsToMetric(value) = 
                let cmToMeter = 0.01
                let inchToMeter = 0.0254
                if (units = UnitsOfMeasurement.Inches) then value * inchToMeter else value * cmToMeter
            let (products, planograms) = parse unitsToMetric yAutofill Map.empty [] t
            {
                ProjectName = projectName
                Products = products
                Planograms = planograms
                UnitsOfMeasurement = units
            }
            |> Some
        | [] -> None
        | _::t -> parseProject t         
            
    let lines = text.Split([|"\n"; "\r\n"|], StringSplitOptions.RemoveEmptyEntries) |> List.ofArray
    parseProject lines