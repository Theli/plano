[<AutoOpen>]
module SharedExtensions

let uncurry f (a, b) = f a b

let ignore2 _ _ = ()

module Math = 
    let inline (.*) x y = 
        float x * float y
    let nearEquals =
        let Epsilon = 0.00001
        fun (x: float) y -> ((x - y)  |> System.Math.Abs) < Epsilon
    let notNearEquals (x: float) y = 
        nearEquals x y
        |> not
    // let inline clamp minV maxV v = v |> max minV |> min maxV
    let inline clamp min max value = if value > max then max else if value < min then min else value
    let divideBy (divider: float) arg = arg / divider
    let extract (toExtract: float) arg = arg - toExtract
module Option = 
    open FSharp.Core
    let inline contains v = function | Some ev -> ev = v | _ -> false

    let inline (>>=) m f =
        Option.bind f m

    let inline (<!>) m f =
        Option.map f m

[<RequireQualifiedAccess>]
module String = 
    //let split sep (str: string) = str.Split([|sep|])

    let withNixLineEndings (str: string) = str.Replace("\r\n", "\n").Replace('\r', '\n')

    let withSpacesInsteadOfTabs spacesPerTab (str: string) = 
        let replacement = " " |> String.replicate spacesPerTab
        str.Replace("\t", replacement)

    let startsWith (prefix: string) (str: string) = 
        let rec compare pos = 
            pos = prefix.Length || (prefix.[pos] = str.[pos] && compare (pos + 1))
        prefix.Length <= str.Length && compare 0

    let lineCount (str: string) =
        let length = str.Length
        let rec calculate count cur =
            if cur = length then count
            else
                match str.[cur] with
                | '\n' -> calculate (count + 1) (cur + 1)
                | _ -> calculate count (cur + 1)
        calculate 1 0            

    let splitByChar (separator: char) (s: string) =
        s
            .Replace("\\\\", "#uslash12745#")
            .Replace("\\,", "#ucomma63891#")
            .Replace("#uslash12745#", "\\")
            .Replace("\\\"", "\"")
            .Split(separator)
            |> Array.map (fun w -> if w.Contains("#ucomma63891#") then w.Replace("#ucomma63891#", ",") else w)

[<AutoOpen>]
module Tuple = 
    type T = T with
        static member (.--) (T, (x1,_)) = x1
        static member (.--) (T, (x1,_,_)) = x1
        static member (.--) (T, (x1,_,_,_)) = x1
        static member (.--) (T, struct (x1,_)) = x1
        static member (.--) (T, struct (x1,_,_)) = x1
        static member (.--) (T, struct (x1,_,_,_)) = x1
        static member (-.-) (T, (_,x2)) = x2
        static member (-.-) (T, (_,x2,_)) = x2
        static member (-.-) (T, (_,x2,_,_)) = x2
        static member (-.-) (T, struct (_,x2)) = x2
        static member (-.-) (T, struct (_,x2,_)) = x2
        static member (-.-) (T, struct (_,x2,_,_)) = x2
        static member (--.) (T, (_,_,x3)) = x3
        static member (--.) (T, (_,_,x3, _)) = x3
        static member (--.) (T, struct (_,_,x3)) = x3
        static member (--.) (T, struct (_,_,x3, _)) = x3
        
    let inline first x = T .-- x
    let inline second x = T -.- x
    let inline third x = T --. x        

        
// [<RequireQualifiedAccess>]
// module Char = 
//     let separatorsAndPunctuation = 
//         seq {
//             for i = System.Char.MinValue to System.Char.MaxValue do
//                 if System.Char.IsSeparator i || System.Char.IsPunctuation i || System.Char.IsWhiteSpace i then yield i
//         } |> Array.ofSeq

[<RequireQualifiedAccess>]           
module Async =

    let result = async.Return

    let map f asyncValue = async {
        let! v = asyncValue
        return f v
    }

    let bind f asyncValue = async {
        let! v = asyncValue
        return! f v
    }


[<RequireQualifiedAccess>]
module Memoization = 
    
    let memoize (f: 'a -> 'b) =
        let dict = new System.Collections.Generic.Dictionary<_,'b>()
        fun a -> 
            match dict.TryGetValue a with
            | true, v -> v
            | _ -> 
                let v = f a
                dict.Add(a, v)
                v
    let memoize2 (f: 'a0 -> 'a1 -> 'b) =
        let dict = new System.Collections.Generic.Dictionary<_,'b>()
        fun a0 (a1: 'a1) -> 
            match dict.TryGetValue((a0, a1)) with
            | true, v -> v
            | _ -> 
                let v = f a0 a1
                dict.Add((a0, a1), v)
                v
//     let memoize2 (f: 'a0 -> 'a1 -> 'b) =
//         let dict = new System.Collections.Concurrent.ConcurrentDictionary<_,'b>()
//         fun a0 a1 -> dict.GetOrAdd((a0, a1), uncurry f)    

[<AutoOpen>]
module Equality = 
    open NonStructuralComparison
    let inline (==) a b = a = b
    let inline (!=) a b = not (a = b)

module OptionOperators = 
    let inline (>>=) m f =
        Option.bind f m

    let inline (<!>) m f =
        Option.map f m


        