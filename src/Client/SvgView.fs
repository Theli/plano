module SvgView

open Fable.Import
open Fable.FileInput
open Planograms.Model
open Fable.Helpers
open React
open React.Props
open Fable
open AppModel
open Geometry

let log = Browser.console.log
[<Literal>]
let DashWidth = 0.005
let drawProductGroup select isSelected (pg: PositionedProductGroup) = 
    let colorString = pg.ProductGroup.Product.Color |> sprintf "#%06X"
    let tooltip =
        let asString = function
            | ProductName s -> sprintf "Name: %s" s
            | Manufacturer s -> sprintf "Manufacturer: %s" s
            | Category s -> sprintf "Category: %s" s
            | Supplier s -> sprintf "Supplier: %s" s 
            | Price p -> sprintf "Price: %.2f" p
            | Brand s -> sprintf "Brand: %s" s
            | SubCategory s -> sprintf "SubCategory: %s" s
            
        pg.ProductGroup.Product.ProductInfo
        |> Seq.map asString
        |> String.concat "\n"
        |> sprintf "UPC: %s\nPOS: %A\n%s" pg.ProductGroup.Product.Upc pg.LocalPosition

    let individualProducts = 
        pg.IndividualProductViews
        |> List.distinctBy (fun p -> {X = p.LocalPosition.X; Y = p.LocalPosition.Y})
        |> List.sortBy (fun p -> p.LocalPosition.Z)
        |> List.map (fun singleProduct -> 
            g [][
                rect 
                    [
                        SVGAttr.Width singleProduct.RotatedSize.X
                        SVGAttr.Height singleProduct.RotatedSize.Y                       
                        X (singleProduct.LocalPosition.X + pg.LocalPosition.X)
                        Y (singleProduct.LocalPosition.Y + pg.LocalPosition.Y)
                        SVGAttr.Fill colorString
                        SVGAttr.Custom ("className", "single-product")
                        SVGAttr.Custom ("vectorEffect", "non-scaling-stroke")
                    ][]])
        |> g []
    
    let productGroupDecorators = 
        match pg.Decorators with
        | Some (Spikes (_, spikePositions)) -> 
            spikePositions
            |> List.map  (fun spikePos -> 
                rect 
                    [
                        SVGAttr.Width DashWidth
                        SVGAttr.Height DashWidth
                        X (spikePos.X - DashWidth / 2.)
                        Y (spikePos.Y - DashWidth / 2.)
                        SVGAttr.Fill "white"
                        SVGAttr.Stroke "none"
                    ][])          
        | _ -> []
    g [] [
        yield individualProducts
        yield! productGroupDecorators
        yield rect 
            [
                SVGAttr.Width pg.TotalSize.X
                SVGAttr.Height pg.TotalSize.Y                 
                X pg.LocalPosition.X 
                Y pg.LocalPosition.Y 
                SVGAttr.Fill "#00000000"
                SVGAttr.Custom ("className", if isSelected then "selected" else "product-group")
                SVGAttr.Custom ("vectorEffect", "non-scaling-stroke")        
                OnClick (fun _ -> select())                    
            ]
            [title [] [str tooltip]
        ]  
    ]

open Planograms.Calculations.CanCombineShelves     
open Planograms.Calculations.Autofill     
open Planograms.Calculations.SpikeFixture
open Planograms.Calculations.Planogram

let drawFixture dispatch selection (planogram: Planogram) (fixture: Fixture) fixturePos (positionedProductGroups: PositionedProductGroup list) = 
    let planoHeight = getPlanogramHeight planogram.RawSize planogram.Fixtures
    let autofillSpace = 
        if fixture.IsAutofillEnabled then Some(findAutofillSpace planogram.Fixtures planoHeight fixture.Size.X fixturePos )
        else None
    let isCombined = isShelfCombined fixture fixturePos planogram.Fixtures
    let colorString =  //todo remove debug later
        if isCombined then 
            "red"
        else 
            fixture.Color |> sprintf "#%06X"   

    let tooltip = sprintf "Layout: %A\nSize: %.2f x %.2f x %.2f\nPosition: %A\nAutofill space: %O" fixture.ProductGroups fixture.Size.X fixture.Size.Y fixture.Size.Z fixturePos autofillSpace 
    let isFixtureSelected = 
        match selection with
        | Fixture (f, _, _) when f = fixture -> true
        | _ -> false
    let isProductGroupSelected index = 
        match selection with
        | ProductGroup(s, i) when s = fixture && i = index -> true
        | Product(s, i) when s = fixture && i = index -> true
        | _ -> false

    let fixtureDecorators = 
        match fixture.ProductGroups with
        | SpikeFixture pegOrSlatWall ->
            let lineAmount = rowCount pegOrSlatWall.SpacingBetweenRows pegOrSlatWall.Offset.Y fixture.Size.Y
            let (lineLength, dashSpacing) = 
                match pegOrSlatWall with
                | Pegboard (spacing, offset, _) -> 
                    let xHoleAmount = holesInRowCount spacing.X offset.X fixture.Size.X
                    let lineLength = spacing.X * (float xHoleAmount)
                    let dashSpacing = (lineLength - (float xHoleAmount) * DashWidth) / (float xHoleAmount)
                    lineLength, dashSpacing
                | SlatWall (_, offset, _) ->  (fixture.Size.X - offset.X), 0.
                | Bar (_) ->  fixture.Size.X, 0.

            let lineX = pegOrSlatWall.Offset.X + fixturePos.X - DashWidth / 2.
            let lineY = fixturePos.Y + (distanceBetweenLastRowAndFixtureBottom pegOrSlatWall.SpacingBetweenRows pegOrSlatWall.Offset.Y fixture.Size.Y)
            let dasharray = sprintf "%f,%f" DashWidth dashSpacing

            [for i in 0 .. (lineAmount - 1) do
                let d = sprintf "M %f %f\nh %f" lineX (lineY + float i * pegOrSlatWall.SpacingBetweenRows) lineLength
                yield path
                    [
                        SVGAttr.D d
                        SVGAttr.StrokeWidth DashWidth
                        SVGAttr.Stroke "black"                        
                        SVGAttr.Fill "none"  
                        SVGAttr.StrokeDasharray dasharray                    
                    ][]]                    
        | _ -> []
    g [] [        
        yield rect 
            [
                SVGAttr.Width fixture.Size.X
                SVGAttr.Height fixture.Size.Y
                X fixturePos.X
                Y fixturePos.Y 
                SVGAttr.Fill colorString
                SVGAttr.Custom ("vectorEffect", "non-scaling-stroke")                
            ][]
        yield! fixtureDecorators
        yield rect 
            [
                SVGAttr.Width fixture.Size.X
                SVGAttr.Height fixture.Size.Y
                X fixturePos.X
                Y fixturePos.Y 
                SVGAttr.Fill "#00000000"
                SVGAttr.Custom ("className", if isFixtureSelected then "selected" else "shelf")
                SVGAttr.Custom ("vectorEffect", "non-scaling-stroke")
                OnClick (fun _ -> fixture |> SelectFixture |> dispatch)
            ] 
            [
                title[] [str tooltip]
            ]

        for i, pg in List.indexed positionedProductGroups do 
            let select () = (fixture, i) |> SelectProductGroup |> dispatch
            let updatedLocalPosition = {X = pg.LocalPosition.X; Y = pg.LocalPosition.Y; Z = 0.} + fixturePos
            let updatedDecorators = 
                match pg.Decorators with
                | Some (Spikes (spikeLength, spikePositions)) -> 
                    spikePositions
                    |> List.map ((+) fixturePos)
                    |> fun p -> Some (Spikes (spikeLength, p))                   
                | _ -> None
            yield drawProductGroup select (isProductGroupSelected i) {pg with LocalPosition = updatedLocalPosition; Decorators = updatedDecorators}
    ]


open Planograms.Calculations.FixtureLayout
let drawPlanogram dispatch selection index (planogram: Planogram) = 
    let (planoMin, _) = findMinMaxPoints planogram.RawSize planogram.Fixtures
    let planoSize = getPlanogramSize planogram.RawSize planogram.Fixtures

    // svg axes flip  https://gist.github.com/fospathi/0ff9f8bb878b6f9c0945b06b62d54d7a
    let isPlanogramSelected = selection |> function | PlanogramVariant _ -> true | Project _ -> true | _ -> false
    let tooltip = sprintf "Planogram" 

    div []
        [
            svg [ViewBox (sprintf "%f 0 %f %f" planoMin.X planoSize.X planoSize.Y); SVGAttr.Height 600] 
                [
                    g 
                        [SVGAttr.Transform (sprintf "matrix(1 0 0 -1 0 %f)" planoSize.Y)] 
                        [
                            yield rect 
                                [
                                    SVGAttr.Width planoSize.X
                                    SVGAttr.Height planoSize.Y
                                    X planoMin.X
                                    Y 0.
                                    SVGAttr.Fill "#dadadb"
                                    SVGAttr.Custom ("className", if isPlanogramSelected then "selected" else "shelf")
                                    SVGAttr.Custom ("vectorEffect", "non-scaling-stroke")
                                    OnClick (fun _ -> if isPlanogramSelected |> not then SelectPlanogram |> dispatch)
                                ] 
                                [
                                    title[] [str tooltip]
                                ]
                            
                            for shelf, pos, pgs in (planogram.Fixtures |> getPositionedProductGroups planoSize.Y) -> 
                                drawFixture dispatch selection planogram shelf pos pgs                           
                        ]
                ]                
        ]