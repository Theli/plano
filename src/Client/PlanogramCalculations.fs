module Planograms.Calculations

open Geometry
open Planograms.Model
open SharedExtensions.Math


module SpikeFixture = 
    
    let rowCount (spacingBetweenRows: float) yOffset fixtureHeight = 
            ((fixtureHeight - yOffset) / spacingBetweenRows) |> ceil |> int |> max 1  
    let holesInRowCount (xSpacing: float) xOffset fixtureWidth = 
        ((fixtureWidth - xOffset) / xSpacing) |> ceil |> int |> max 1      

    let distanceBetweenLastRowAndFixtureBottom spacingY offsetY (fixtureHeight: float) = 
        (fixtureHeight - offsetY) % spacingY    

    let relativeSpikePosition product rotatedSize = 
        let relX = 
            match product.MainSpikeOffset.X with
            | x when x |> nearEquals 0. -> rotatedSize.X / 2.
            | x when x > rotatedSize.X -> 0.
            | x -> x   
        let relY = 
            let DEFAULT_PEG_Y_POSITION = 0.008
            match product.MainSpikeOffset.Y with
            | y when y |> nearEquals 0. -> DEFAULT_PEG_Y_POSITION
            | y -> y      
        let topLeftSpikeOffset = {X = relX; Y = -relY; Z = 0.}           
        let topLeftProductCorner =  {X = 0.; Y = rotatedSize.Y; Z = 0.}
        topLeftProductCorner + topLeftSpikeOffset  

    let mainSpikePositionAndItsLine (spacing: Vector2) (offset: Vector2) shelfSize productGroup pgPos =  
        let distanceBetweenLastRowAndFixtureBottom = (distanceBetweenLastRowAndFixtureBottom spacing.Y offset.Y shelfSize.Y)
        let mainSpike = 
            rotatedProductSize productGroup.Product productGroup.Orientation
            |> relativeSpikePosition productGroup.Product
            |> ((+) pgPos)
            |> fun p ->            
                let clampedX = p.X |> clamp offset.X shelfSize.X
                let clampedY = p.Y |> clamp distanceBetweenLastRowAndFixtureBottom (shelfSize.Y - offset.Y)
                { X = clampedX; Y = clampedY }
        
        let lineNumber = (mainSpike.Y - distanceBetweenLastRowAndFixtureBottom) |> divideBy spacing.Y |> ceil |> int                      
        mainSpike, lineNumber

module CanCombineShelves =
    let isShelfCombined shelf shelfPos shelves = 
        match shelf.ProductGroups with
        | Auto (layout, _) when shelf.CanCombine ->
            shelves
            |> List.exists (fun (otherShelf, otherPosition) -> 
                match otherShelf.ProductGroups with
                | Auto (otherLayout, _) when otherShelf.CanCombine && layout = otherLayout -> 
                    nearEquals (shelfPos.Y + shelf.Size.Y) (otherPosition.Y + otherShelf.Size.Y)
                    && nearEquals shelf.Size.Z otherShelf.Size.Z
                    && (nearEquals shelfPos.X (otherPosition.X + otherShelf.Size.X) || nearEquals (shelfPos.X + shelf.Size.X) otherPosition.X)
                | _ -> false)
        | _ -> false                

    let groupCombinedShelves shelves = 
        let findCombinedShelves shelf shelfPos shelves =      
            let rec findRight acc (shelf, shelfPos) = 
                let snappedRight = 
                    match shelf.ProductGroups with
                    | Auto (layout, _) when shelf.CanCombine ->
                        shelves
                        |> List.tryPick (fun (otherShelf, otherPosition) -> 
                            match otherShelf.ProductGroups with
                            | Auto (otherLayout, _) when otherShelf.CanCombine && layout = otherLayout 
                                && nearEquals shelf.Size.Z otherShelf.Size.Z
                                && nearEquals (shelfPos.Y + shelf.Size.Y) (otherPosition.Y + otherShelf.Size.Y)
                                && nearEquals (shelfPos.X + shelf.Size.X) otherPosition.X -> Some(otherShelf, otherPosition)
                            | _ -> None)
                    | _ -> None    

                match snappedRight with
                | Some (rightShelf, rightPos)-> findRight ((rightShelf, rightPos)::acc) (rightShelf, rightPos)
                | _ -> acc |> List.rev
            let rec findLeft acc (shelf, shelfPos) = 
                let snappedLeft = 
                    match shelf.ProductGroups with
                    | Auto (layout, _) when shelf.CanCombine ->
                        shelves
                        |> List.tryPick (fun (otherShelf, otherPosition) -> 
                            match otherShelf.ProductGroups with
                            | Auto (otherLayout, _) when otherShelf.CanCombine && layout = otherLayout  
                                && nearEquals (shelfPos.Y + shelf.Size.Y) (otherPosition.Y + otherShelf.Size.Y)
                                && nearEquals shelfPos.X (otherPosition.X + otherShelf.Size.X) -> Some (otherShelf, otherPosition)
                            | _ -> None)
                    | _ -> None    

                match snappedLeft with
                | Some (leftShelf, leftPos) -> findLeft ((leftShelf, leftPos)::acc) (leftShelf, leftPos)
                | _ -> acc

            ((findLeft [] (shelf, shelfPos)),(findRight [] (shelf, shelfPos)))   

        shelves
        |> List.map (fun (shelf, shelfPos) -> 
            let (leftConnectedShelves, rightConnectedShelves) = findCombinedShelves shelf shelfPos shelves
            [
                yield! leftConnectedShelves
                yield (shelf, shelfPos)
                yield! rightConnectedShelves
            ])
        |> List.distinct
            
module Autofill = 
    let findAutofillSpace shelves planogramHeight targetWidth targetPosition =    
        shelves
        |> List.fold (fun autofillY (otherShelf, otherPosition) ->
            if otherPosition.Y > targetPosition.Y 
                && otherPosition.X <= (targetPosition.X + targetWidth) 
                && (otherPosition.X + otherShelf.Size.X) >= targetPosition.X 
                && otherPosition.Y < autofillY
            then 
                otherPosition.Y
            else 
                autofillY) planogramHeight
        |> fun autofillY -> autofillY - targetPosition.Y
        |> max 0.

module FixtureLayout = 
    open Autofill
    open SpikeFixture
    open CanCombineShelves
    let private internalSpacingInSpacedLayout shelfSize (productGroups: ProductGroup list) =                
        match productGroups with
        | [] -> Vector3.Zero
        | [productGroup] ->
            match productGroup.XMainAndCappingCount with
            | 1 -> Vector3.Zero
            | x -> { X =  (shelfSize.X - productGroup.Width) / float (x - 1); Y = 0.; Z = 0.}               
        | other ->                    
            let productCount = other |> List.sumBy (fun pg -> pg.XMainAndCappingCount)
            let productGroupsWidth = other |> List.sumBy (fun pg -> pg.Width)
            let gap = (shelfSize.X - productGroupsWidth) / float(productCount - 1)
            { X = gap; Y = 0.; Z = 0.}

    let getXPositionedShelfProductGroups shelfSize (layout, (productGroups: ProductGroup list))  = 
        match layout with
        | SnapLeft ->
            productGroups 
            |> List.fold (fun (offset, positionedProductGroups) productGroup -> 
                offset + productGroup.Width, (productGroup, offset)::positionedProductGroups) (0., [])
            |> second
            |> List.rev
        | SnapRight ->
            productGroups
            |> List.rev
            |> List.fold (fun (offset, positionedProductGroups) productGroup -> 
                offset - productGroup.Width, (productGroup, offset - productGroup.Width)::positionedProductGroups) (shelfSize.X, [])
            |> second
        | Spread ->
            match productGroups with
            | [] -> []
            | [productGroup] ->
                [(productGroup, (shelfSize.X - productGroup.Width) / 2.) ]
            | other -> 
                let productGroupsCount = other |> List.length
                let productGroupsWidth = other |> List.sumBy (fun pg -> pg.Width)
                let gap = (shelfSize.X - productGroupsWidth) / float (productGroupsCount - 1)
                other 
                |> List.fold (fun (offset, positionedProductGroups) productGroup -> 
                    offset + productGroup.Width + gap, (productGroup, offset)::positionedProductGroups) (0., [])
                |> second
                |> List.rev
        | Spaced ->
            match productGroups with
            | [] -> []
            | [productGroup] when productGroup.Count |> first  = 1 && productGroup.XCapping.IsNone ->
                [(productGroup, (shelfSize.X - productGroup.Width) / 2.) ]
            | [productGroup] ->
                [(productGroup, 0.) ]
            | other -> 
                let internalSpacing = internalSpacingInSpacedLayout shelfSize productGroups
                other 
                |> List.fold (fun (offset, positionedProductGroups) productGroup -> 
                    let nextOffset = offset + productGroup.Width + productGroup.XMainAndCappingCount .* internalSpacing.X
                    nextOffset, (productGroup, offset)::positionedProductGroups) (0., [])
                |> second
                |> List.rev
              
    let private getPositionedAutofilledProductGroups getAutofillSpace shelf = 
        match shelf.ProductGroups with
        | Manual positionedProductGroups -> 
            positionedProductGroups 
            |> List.map (fun (pg, offset) -> 
                let localPosition = { X = offset; Y = shelf.Size.Y; Z = shelf.Size.Z }
                let autofillSpace = getAutofillSpace pg.Width localPosition
                let (individualProducts, pgTotalSize) = pg.ShelfProductGroup Vector3.Zero autofillSpace
                {
                    IndividualProductViews = individualProducts
                    ProductGroup = pg
                    LocalPosition = localPosition
                    TotalSize = pgTotalSize         
                    Decorators = None          
                })
        | Auto (layout, productGroups) ->
            let internalSpacing = 
                if layout = Spaced then internalSpacingInSpacedLayout shelf.Size productGroups                
                else Vector3.Zero
            (layout, productGroups)
            |> getXPositionedShelfProductGroups shelf.Size
            |> List.map (fun (pg, xPos) -> 
                let localPosition = { X = xPos; Y = shelf.Size.Y; Z = shelf.Size.Z }      
                let autofillSpace = getAutofillSpace pg.Width localPosition
                let (individualProducts, pgTotalSize) = pg.ShelfProductGroup internalSpacing autofillSpace
                {
                    IndividualProductViews = individualProducts
                    ProductGroup = pg
                    LocalPosition = localPosition
                    TotalSize = pgTotalSize          
                    Decorators = None        
                })    

        | SpikeFixture (Bar positionedProductGroups) -> 
            positionedProductGroups
            |> List.map (fun (pg, xOffset) -> 
                let rotatedSize = rotatedProductSize pg.Product pg.Orientation
                let mainSpikeOffset = relativeSpikePosition pg.Product rotatedSize

                let localPosition = 
                    {
                        X = xOffset - mainSpikeOffset.X
                        Y = shelf.Size.Y - mainSpikeOffset.Y   
                        Z = rotatedSize.Z .* (pg.Count |> third) + shelf.Size.Z
                    }
                let (individualProducts, pgTotalSize) = pg.SpikeFixtureProductGroup 0. None
                let spikeLength = pgTotalSize.Z
                let spikes =                     
                    individualProducts
                    |> List.distinctBy (fun p -> {X = p.LocalPosition.X; Y = p.LocalPosition.Y})
                    |> List.map (fun p -> p.LocalPosition + mainSpikeOffset + localPosition)
                {
                    IndividualProductViews = individualProducts
                    ProductGroup = pg
                    LocalPosition = localPosition
                    TotalSize = pgTotalSize
                    Decorators = Some (Spikes (spikeLength, spikes))                   
                }) 
        | SpikeFixture pegOrSlat -> 
            let (spacingBetweenRows, horizontalSpacingInRow, holesOffset, positionedProductGroups) = 
                match pegOrSlat with
                | Pegboard (spacing, offset, positionedProductGroups) ->
                    let pgs = 
                        positionedProductGroups
                        |> List.map (fun (pg, (xHole, yHole)) -> pg, (yHole, float xHole * spacing.X))
                    spacing.Y, Some spacing.X, offset, pgs
                | SlatWall (ySpacing, offset, positionedProductGroups) ->
                    ySpacing, None, offset, positionedProductGroups
                | _ -> failwith "unexpected"

            positionedProductGroups
            |> List.map (fun (pg, (rowNumber, xOffset)) -> 
                let rotatedSize = rotatedProductSize pg.Product pg.Orientation
                let mainSpikeOffset = relativeSpikePosition pg.Product rotatedSize
                let yOffset = (float rowNumber * spacingBetweenRows) 
                let distanceBetweenLastRowAndFixtureBottom = distanceBetweenLastRowAndFixtureBottom pegOrSlat.SpacingBetweenRows pegOrSlat.Offset.Y shelf.Size.Y
                let localPosition = 
                    {
                        X = xOffset + holesOffset.X - mainSpikeOffset.X
                        Y = yOffset + distanceBetweenLastRowAndFixtureBottom - mainSpikeOffset.Y   
                        Z = rotatedSize.Z .* (pg.Count |> third) + shelf.Size.Z
                    }
                let (individualProducts, pgTotalSize) = pg.SpikeFixtureProductGroup spacingBetweenRows horizontalSpacingInRow
                let spikeLength = pgTotalSize.Z
                let spikes =                     
                    individualProducts
                    |> List.distinctBy (fun p -> {X = p.LocalPosition.X; Y = p.LocalPosition.Y})
                    |> List.map (fun p -> p.LocalPosition + mainSpikeOffset + localPosition)
                {
                    IndividualProductViews = individualProducts
                    ProductGroup = pg
                    LocalPosition = localPosition
                    TotalSize = pgTotalSize
                    Decorators = Some (Spikes (spikeLength, spikes))                   
                }) 


    let getPositionedProductGroups planogramHeight shelves = 
        let getAutofillSpace (shelf, shelfPos) = 
            fun pgWidth pgLocalPos ->
                if shelf.IsAutofillEnabled then 
                    Some (findAutofillSpace shelves planogramHeight pgWidth (pgLocalPos + shelfPos + {X = 0.;Y = shelf.Size.Y; Z = 0.}))
                else None
        shelves
        |> groupCombinedShelves
        |> List.collect(function
            | [(singleShelf, pos)] -> 
                let positionedProductGroups = getPositionedAutofilledProductGroups (getAutofillSpace (singleShelf, pos)) singleShelf       
                [(singleShelf, pos, positionedProductGroups)]
            | (leftmostShelf, leftmostShelfPos)::_ as groupedShelves ->
                let combinedWidth = groupedShelves |> List.sumBy (fun (s, _) -> s.Size.X)
                let productGroups = 
                    match leftmostShelf.ProductGroups with
                    | Auto (layout, _) -> 
                        Auto(layout, groupedShelves |> List.collect (fun (s,_) -> s.ProductGroups.ProductGroupList))
                    | _ -> failwith "unexpected"            
                let tmpCombinedShelf = {leftmostShelf with Size = {leftmostShelf.Size with X = combinedWidth}; ProductGroups = productGroups }   
                let getAutofillSpace = getAutofillSpace (tmpCombinedShelf, leftmostShelfPos)
                groupedShelves
                |> List.fold (fun (acc, ppgs) (shelf, shelfPos) -> 
                    let pgCount = shelf.ProductGroups.ProductGroupList |> List.length
                    let xCombinedShelfOffset = leftmostShelfPos.X - shelfPos.X
                    let productGroups = 
                        ppgs 
                        |> List.take pgCount
                        |> List.map (fun (pg: PositionedProductGroup) -> {pg with LocalPosition = {pg.LocalPosition with X = pg.LocalPosition.X + xCombinedShelfOffset}})
                    (shelf, shelfPos, productGroups)::acc, (ppgs |> List.skip pgCount)) ([], (getPositionedAutofilledProductGroups getAutofillSpace tmpCombinedShelf))
                |> first
            | _ -> [])

module Planogram =
    open FixtureLayout
    let findMinMaxPoints = 
        fun knownSize fixtures -> 

            fixtures
            |> getPositionedProductGroups knownSize.Y
            |> List.fold (fun (minPoint, maxPoint) (shelf, shelfPos, positionedProductGroups) -> 
                let (shelfRelativeMin, shelfRelativeMax) = 
                    positionedProductGroups
                    |> List.fold (fun (minPoint, maxPoint) pg -> 
                        (Vector3.Min minPoint pg.LocalPosition), (Vector3.Max maxPoint (pg.LocalPosition+pg.TotalSize)))
                        (Vector3.MaxValue, Vector3.MinValue)
                    |> fun (minPoint, maxPoint) ->
                        (Vector3.Min minPoint Vector3.Zero), (Vector3.Max maxPoint shelf.Size)

                (Vector3.Min minPoint (shelfRelativeMin + shelfPos)), (Vector3.Max maxPoint (shelfRelativeMax + shelfPos)))
                (Vector3.MaxValue, Vector3.MinValue)
            |> fun (minPoint, maxPoint) -> 
                (Vector3.Min minPoint Vector3.Zero), (Vector3.Max maxPoint Vector3.Zero)
        |> Memoization.memoize2
    let getPlanogramSize = 
        fun knownSize fixtures -> 
            let (min, max) = findMinMaxPoints  knownSize fixtures
            max - min
        |> Memoization.memoize2

    let getPlanogramHeight knownSize fixtures = 
        (getPlanogramSize knownSize fixtures).Y
       