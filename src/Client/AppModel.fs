module AppModel

open Fable.Import
open Fable.FileInput
open Planograms.Model
open Geometry

let log = Browser.console.log

type Selection = 
    | Nothing
    | Fixture of Fixture * searchNewProductText: string * searchExistingProductText: string
    | ProductGroup of Fixture * index: int
    | Product of Fixture * index: int
    | PlanogramVariant 
    | Project of searchText: string

type State = 
    | NotLoaded
    | Loaded of PlanogramProject * planoIndex: int * Selection

type MoveProductGroupMsg = 
    | MoveOnAutoLayout of Fixture * oldIndex: int * newIndex: int
    | MoveOnManualLayout of Fixture * index: int * xOffset: float
    | MoveOnPegboard of Fixture * index: int * xHole: int * yHole: int  
    | MoveOnSlatwall of Fixture * index: int * lineNumber: int * xOffset: float  
    | MoveOnBar of Fixture * index: int * xOffset: float  
    

type PlanogramMsg = 
    | SelectFixture of Fixture
    | SelectProductGroup of Fixture * index: int
    | SelectProduct of Fixture * index: int
    | MoveFixture of Fixture * Vector3
    | ResizeFixture of Fixture * Vector3
    | RenameFixture of Fixture * string
    | SetShelfAutofillEnabled of Fixture * bool
    | SetShelfCanCombineEnabled of Fixture * bool
    | DeleteProductGroup of Fixture * index: int
    | ToManualLayout of Fixture
    | ToAutoLayout of AutoLayout * Fixture
    | EditSlatwall of spacingBetweenRows: float * offset: Vector2 * slatwall: Fixture
    | EditPegboard of spacing: Vector2 * offset: Vector2 * pegboard: Fixture 
    | ChangePlanogramVariant of int
    | SelectPlanogram
    | SelectPlanogramProject
    | SearchForExistingProductGroupByUpcOrName of string
    | SearchForProductUpcOrName of string
    | ModifyProductGroup of updatedGroup: ProductGroup * Fixture * productGroupIndex: int
    | MoveProductGroup of MoveProductGroupMsg
    | CreateProductGroup of Fixture * Product
    | DuplicateFixture of Fixture * Vector3 * FixtureDuplicateOptions
    | RemoveFixture of Fixture * Vector3

type Msg = 
    | LoadPsa of FileInfo<string>
    | PlanogramMsg of PlanogramMsg
