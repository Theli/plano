module FableScaffold

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import
open Fable.FileInput
open Planograms.Model
open Planograms
open Fable.Helpers
open AppModel
open AppView
open Geometry
open Planograms.Calculations.Planogram
open Planograms.Calculations.SpikeFixture
open Planograms.Calculations.FixtureLayout
open SharedExtensions.Math
let log = Browser.console.log
let init () = NotLoaded


let duplicateShelf (shelf, position) shelfDuplicateOptions (plano: Planogram)  = 
    let planoSize = getPlanogramSize plano.RawSize plano.Fixtures
    let newPosition = {X = position.X; Y = planoSize.Y; Z = position.Z}
    let updatedProductGroups = 
        if shelfDuplicateOptions = WithProducts then shelf.ProductGroups
        else 
            match shelf.ProductGroups with
            | Manual _ -> Manual[]
            | Auto (l, _) -> Auto(l,[])
            | SpikeFixture (Pegboard (spacing, offset, _)) -> SpikeFixture (Pegboard (spacing, offset, []))
            | SpikeFixture (SlatWall (spacing, offset, _)) -> SpikeFixture (SlatWall (spacing, offset, []))
            | SpikeFixture (Bar _) -> SpikeFixture (Bar [])
    let newShelf = {shelf with ProductGroups = updatedProductGroups; GeneratedId = random.Next()} 
    newShelf, newPosition

let removeShelf (shelf, position) (plano: Planogram) = 
    let shelves = 
        plano.Fixtures |> List.filter (fun (s, p) -> s <> shelf || p <> position )
    Planogram(plano.PlanogramName, plano.RawSize, shelves) 

        
let editShelf oldShelf newShelfOption (plano: Planogram) = 
    let shelves = 
        plano.Fixtures |> List.choose (fun (s, p) -> 
            if s <> oldShelf then Some(s, p) 
            else 
                match newShelfOption with
                | Some ns ->  Some(ns, p)
                | _ -> None)
    Planogram(plano.PlanogramName, plano.RawSize, shelves)        

let moveShelf shelf newPosition (plano: Planogram) = 
    let shelves = 
        plano.Fixtures |> List.map (fun (s, p) -> if s = shelf then (s, newPosition) else (s, p))
    Planogram(plano.PlanogramName, plano.RawSize, shelves) 

let updateProductGroupeMove moveMsg (planoProject: PlanogramProject, index, selection) = 
    let currentPlanogram = planoProject.Planograms |> List.item index 
    let oldAndUpdatedShelfAndNewSelection =   
        match moveMsg with
        | MoveOnAutoLayout (shelf, oldIndex, newIndex) when oldIndex <> newIndex-> 
            match shelf.ProductGroups with
            | Auto (layout, pgs) ->
                let targetPG = pgs |> List.item oldIndex 
                let updatedPgs =   
                    if newIndex < oldIndex then
                        pgs 
                        |> List.mapi (fun i pg -> if i = newIndex then [targetPG; pg] elif i = oldIndex then [] else [pg])
                        |> List.concat
                    else 
                        pgs 
                        |> List.mapi (fun i pg -> if i = newIndex then [pg; targetPG] elif i = oldIndex then [] else [pg])
                        |> List.concat
                let updatedShelf = {shelf with ProductGroups = Auto(layout, updatedPgs)}        
                (shelf, updatedShelf, (ProductGroup (updatedShelf, newIndex))) |> Some
            |_ -> None
        | MoveOnManualLayout (shelf, pgIndex, xOffset) ->
            match shelf.ProductGroups with
            | Manual pgs ->
                let updatedPgs = 
                    pgs
                    |> List.mapi (fun i (pg, x) -> if i = pgIndex then (pg, xOffset) else (pg, x))
                let updatedShelf = {shelf with ProductGroups = Manual(updatedPgs)}        
                (shelf, updatedShelf, (ProductGroup (updatedShelf, pgIndex))) |> Some         
            | _ -> None    
        | MoveOnPegboard (shelf, pgIndex, newXHole, newYHole) ->
            match shelf.ProductGroups with
            | SpikeFixture (Pegboard (spacing, offset, pgs)) ->
                let newHole = (newXHole, newYHole)
                let updatedPgs = 
                    pgs
                    |> List.mapi (fun i (pg, hole) -> if i = pgIndex then (pg, newHole) else (pg, hole))
                let updatedShelf = {shelf with ProductGroups = SpikeFixture (Pegboard (spacing, offset, updatedPgs))}        
                (shelf, updatedShelf, (ProductGroup (updatedShelf, pgIndex))) |> Some         
            | _ -> None  
        | MoveOnSlatwall (shelf, pgIndex, newLine, newOffset) ->
            match shelf.ProductGroups with
            | SpikeFixture (SlatWall (spacing, offset, pgs)) ->
                let newPosition = (newLine, newOffset)
                let updatedPgs = 
                    pgs
                    |> List.mapi (fun i (pg, pos) -> if i = pgIndex then (pg, newPosition) else (pg, pos))
                let updatedShelf = {shelf with ProductGroups = SpikeFixture (SlatWall (spacing, offset, updatedPgs))}        
                (shelf, updatedShelf, (ProductGroup (updatedShelf, pgIndex))) |> Some         
            | _ -> None  
        | MoveOnBar (shelf, pgIndex, newOffset) ->
            match shelf.ProductGroups with
            | SpikeFixture (Bar pgs) ->
                let updatedPgs = 
                    pgs
                    |> List.mapi (fun i (pg, pos) -> if i = pgIndex then (pg, newOffset) else (pg, pos))
                let updatedShelf = {shelf with ProductGroups = SpikeFixture (Bar updatedPgs)}        
                (shelf, updatedShelf, (ProductGroup (updatedShelf, pgIndex))) |> Some         
            | _ -> None  
        | _ -> None  

    match oldAndUpdatedShelfAndNewSelection with
    | Some (oldShelf, updatedShelf, newSelection) ->

        let shelves =
            currentPlanogram.Fixtures |> List.map (fun (s, p) -> if s = oldShelf then (updatedShelf, p) else (s, p))    
        let updatedPlanogram = Planogram(currentPlanogram.PlanogramName, currentPlanogram.RawSize, shelves) 

        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then updatedPlanogram else p)}

        (updatedPlanoProject, index, newSelection)
    | None -> (planoProject, index, selection) 

let updatePlanogram planogramMsg (planoProject, index, selection) = 
    let currentPlanogram = planoProject.Planograms |> List.item index   
    match planogramMsg with
    | SelectFixture shelf -> (planoProject, index, Fixture (shelf, "", ""))
    | SelectProductGroup (shelf, pgIndex) -> (planoProject, index, ProductGroup (shelf, pgIndex))
    | SelectProduct (shelf, pgIndex) -> (planoProject, index, Product (shelf, pgIndex))
    | MoveFixture (shelf, newPosition) ->
        let plano = 
            currentPlanogram
            |> moveShelf shelf newPosition
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> shelf, searchNew, searchPg
            | _ -> shelf, "", ""
            |> Fixture            
        (updatedPlanoProject, index, updatedSelection)
    | DuplicateFixture (shelf, pos, options) -> 
        let (newShelf, newPosition) = 
            currentPlanogram
            |> duplicateShelf (shelf, pos) options
        let updatedPlanogram = Planogram(currentPlanogram.PlanogramName, currentPlanogram.RawSize, (newShelf, newPosition)::currentPlanogram.Fixtures ) 
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then updatedPlanogram else p)}       
        (updatedPlanoProject, index, Fixture (newShelf, "", ""))
    | RemoveFixture (shelf, pos) when (currentPlanogram.Fixtures |> List.length) > 1-> 
        let plano = 
            currentPlanogram
            |> removeShelf (shelf, pos)
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        (updatedPlanoProject, index, PlanogramVariant )

    | ResizeFixture (shelf, newSize) when newSize.X > 0. && newSize.Y > 0. && newSize.Z > 0. ->
        let updatedShelf =            
            match shelf.ProductGroups with
            | Manual pgs -> 
                let updatedPgs = 
                    pgs
                    |> List.map (fun (pg, xPos) -> pg, (min xPos newSize.X))
                {shelf with Size = newSize; ProductGroups = Manual updatedPgs}
            | Auto _ ->
                {shelf with Size = newSize}
            | SpikeFixture spikeFixture -> 
                let maxRowNumber = ( rowCount spikeFixture.SpacingBetweenRows spikeFixture.Offset.Y newSize.Y) - 1
                match spikeFixture with 
                | Pegboard (spacing, offset, pgs) ->                     
                    let maxHoleNumber = (holesInRowCount spacing.X offset.X newSize.X) - 1
                    let updatedPgs = 
                        pgs
                        |> List.map (fun (pg, (xHole, rowNumber)) ->                        
                            pg, ((xHole |> clamp 0 maxHoleNumber), (rowNumber |> clamp 0 maxRowNumber)))

                    {shelf with Size = newSize; ProductGroups = SpikeFixture (Pegboard (spacing, offset, updatedPgs))}
                | SlatWall (spacingBetweenRows, offset, pgs)  -> 
                    let rowLength = newSize.X - offset.X
                    let updatedPgs = 
                        pgs
                        |> List.map (fun (pg, (rowNumber, xOffset)) ->                        
                            pg, ((rowNumber |> clamp 0 maxRowNumber, (xOffset |> clamp 0. rowLength))))

                    {shelf with Size = newSize; ProductGroups = SpikeFixture (SlatWall (spacingBetweenRows, offset, updatedPgs))}
                | Bar pgs -> 
                    let updatedPgs = 
                        pgs
                        |> List.map (fun (pg, xOffset) ->                        
                            pg,  (xOffset |> clamp 0. newSize.X))

                    {shelf with Size = newSize; ProductGroups = SpikeFixture (Bar updatedPgs)}

        let shelves =
            currentPlanogram.Fixtures |> List.map (fun (s, p) -> if s = shelf then (updatedShelf, p) else (s, p))    
        let updatedPlanogram = Planogram(currentPlanogram.PlanogramName, currentPlanogram.RawSize, shelves) 
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then updatedPlanogram else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
            | _ -> updatedShelf, "", ""
            |> Fixture            
        (updatedPlanoProject, index, updatedSelection)   
    | RenameFixture (shelf, name)  ->
        let updatedShelf = {shelf with FixtureName = name}
        let shelves =
            currentPlanogram.Fixtures |> List.map (fun (s, p) -> if s = shelf then (updatedShelf, p) else (s, p))    
        let updatedPlanogram = Planogram(currentPlanogram.PlanogramName, currentPlanogram.RawSize, shelves) 

        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then updatedPlanogram else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
            | _ -> updatedShelf, "", ""
            |> Fixture            
        (updatedPlanoProject, index, updatedSelection)                
    | SetShelfAutofillEnabled(shelf, isChecked) -> 
        let updatedShelf = {shelf with IsAutofillEnabled = isChecked}
        let shelves =
            currentPlanogram.Fixtures |> List.map (fun (s, p) -> if s = shelf then (updatedShelf, p) else (s, p))    
        let updatedPlanogram = Planogram(currentPlanogram.PlanogramName, currentPlanogram.RawSize, shelves) 


        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then updatedPlanogram else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
            | _ -> updatedShelf, "", ""
            |> Fixture            
        (updatedPlanoProject, index, updatedSelection)       
    | SetShelfCanCombineEnabled(shelf, isChecked) -> 
        let updatedShelf = {shelf with CanCombine = isChecked}
        let shelves =
            currentPlanogram.Fixtures |> List.map (fun (s, p) -> if s = shelf then (updatedShelf, p) else (s, p))    
        let updatedPlanogram = Planogram(currentPlanogram.PlanogramName, currentPlanogram.RawSize, shelves) 


        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then updatedPlanogram else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
            | _ -> updatedShelf, "", ""
            |> Fixture            
        (updatedPlanoProject, index, updatedSelection)       
    | MoveProductGroup msg' -> 
        updateProductGroupeMove msg' (planoProject, index, selection)
    | CreateProductGroup (shelf, product) ->
        let autofillOptions =  {CappingOrientation = {Side = Side.Front; Roll = Roll.Deg0}; MaximumTotal = 99; MinimumTotal = 99; MaximumUnits = 0; MaximumCapping = 0}
        let pg = {Product = product; Orientation = {Side = Side.Front; Roll = Roll.Deg0}; Count = (1, 1, 1); XCapping = None; YCapping = None; ZCapping = None; YAutofill = autofillOptions; MerchStyle = MerchStyle.Unit}
        let updatedShelf = 
            match shelf.ProductGroups with
            | Manual pgs -> 
                {shelf with ProductGroups = Manual ((pg, 0.)::pgs)}
            | Auto (layout, pgs) ->
                {shelf with ProductGroups = Auto (layout, pg::pgs)}
            | SpikeFixture (Pegboard (spacing, offset, pgs)) -> 
                {shelf with ProductGroups = SpikeFixture (Pegboard (spacing, offset, (pg, (0, 0))::pgs))}
            | SpikeFixture (SlatWall (spacing, offset, pgs)) -> 
                {shelf with ProductGroups = SpikeFixture (SlatWall (spacing, offset, (pg, (0, 0.))::pgs))}
            | SpikeFixture (Bar pgs) -> 
                {shelf with ProductGroups = SpikeFixture (Bar ((pg, 0.)::pgs))}

        let plano = 
            planoProject.Planograms |> List.item index
            |> editShelf shelf (Some updatedShelf)
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        (updatedPlanoProject, index, ProductGroup (updatedShelf, 0))

    | DeleteProductGroup (shelf, productGroupIndex) ->
        let updatedShelf = 
            match shelf.ProductGroups with
            | Manual positionedProductGroups -> 
                { shelf with ProductGroups = Manual ([for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos)]) }
            | Auto (layout, productGroups) ->
                { shelf with ProductGroups = Auto(layout, [for i, pg in List.indexed productGroups do if i <> productGroupIndex then yield pg]) }
            | SpikeFixture (Pegboard (spacing, offset, positionedProductGroups)) -> 
                { shelf with ProductGroups = SpikeFixture (Pegboard (spacing, offset, [for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos)])) }             
            | SpikeFixture (SlatWall (spacing, offset, positionedProductGroups)) -> 
                { shelf with ProductGroups = SpikeFixture (SlatWall (spacing, offset, [for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos)]) )}                         
            | SpikeFixture (Bar positionedProductGroups) -> 
                { shelf with ProductGroups = SpikeFixture (Bar ([for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos)]) )}                         
        let plano = 
            planoProject.Planograms |> List.item index
            |> editShelf shelf (Some updatedShelf)
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        (updatedPlanoProject, index, Nothing)

     | ModifyProductGroup (updatedProductGroup, shelf, productGroupIndex) ->
        let updatedShelf = 
            match shelf.ProductGroups with
            | Manual positionedProductGroups -> 
                { shelf with ProductGroups = Manual ([for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos) else yield (updatedProductGroup, pos)]) }
            | Auto (layout, productGroups) ->
                { shelf with ProductGroups = Auto(layout, [for i, pg in List.indexed productGroups do if i <> productGroupIndex then yield pg else yield updatedProductGroup]) }
            | SpikeFixture (Pegboard (spacing, offset, positionedProductGroups)) -> 
                { shelf with ProductGroups = SpikeFixture (Pegboard (spacing, offset, [for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos) else yield (updatedProductGroup, pos)])) }             
            | SpikeFixture (SlatWall (spacing, offset, positionedProductGroups)) -> 
                { shelf with ProductGroups = SpikeFixture (SlatWall (spacing, offset, [for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos) else yield (updatedProductGroup, pos)])) }                         
            | SpikeFixture (Bar positionedProductGroups) -> 
                { shelf with ProductGroups = SpikeFixture (Bar ([for i, (pg, pos) in List.indexed positionedProductGroups do if i <> productGroupIndex then yield (pg, pos) else yield (updatedProductGroup, pos)])) }                         
        let plano = 
            planoProject.Planograms |> List.item index
            |> editShelf shelf (Some updatedShelf)
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        (updatedPlanoProject, index, ProductGroup(updatedShelf, productGroupIndex))

    | ToManualLayout shelf ->
        match shelf.ProductGroups with 
        | Auto (layout, pgs) ->
            let positionedProductGroups = getXPositionedShelfProductGroups shelf.Size (layout, pgs)
            let updatedShelf = {shelf with ProductGroups = Manual positionedProductGroups }
            let plano = 
                planoProject.Planograms |> List.item index
                |> editShelf shelf (Some updatedShelf)
            let updatedPlanoProject = 
                {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
            let updatedSelection = 
                match selection with
                | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
                | _ -> updatedShelf, "", ""
                |> Fixture                
            (updatedPlanoProject, index, updatedSelection)
        | _ ->  (planoProject, index, selection)

    | EditSlatwall (spacingBetweenRows, offset, shelf) ->
        let updatedProductGroups = 
            match shelf.ProductGroups with
            | SpikeFixture (SlatWall (_, _, pgs)) -> 
                let maxRowNumber = ( rowCount spacingBetweenRows offset.Y shelf.Size.Y) - 1
                let rowLength = shelf.Size.X - offset.X
                let updatedPgs = 
                    pgs
                    |> List.map (fun (pg, (rowNumber, xOffset)) ->                        
                        pg, ((rowNumber |> clamp 0 maxRowNumber, (xOffset |> clamp 0. rowLength))))

                SpikeFixture (SlatWall (spacingBetweenRows, offset, updatedPgs))
            | _ -> failwith "unexpected"
        let updatedShelf = {shelf with ProductGroups = updatedProductGroups }
        let plano = 
            planoProject.Planograms |> List.item index
            |> editShelf shelf (Some updatedShelf)
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
            | _ -> updatedShelf, "", ""
            |> Fixture

        (updatedPlanoProject, index, updatedSelection)

    | EditPegboard (spacing, offset, shelf) ->
        let updatedProductGroups = 
            match shelf.ProductGroups with
            | SpikeFixture (Pegboard (_, _, pgs))-> 
                let maxRowNumber = ( rowCount spacing.Y offset.Y shelf.Size.Y) - 1
                let maxHoleNumber = (holesInRowCount spacing.X offset.X shelf.Size.X) - 1
                let updatedPgs = 
                    pgs
                    |> List.map (fun (pg, (xHole, rowNumber)) ->                        
                        pg, ((xHole |> clamp 0 maxHoleNumber), (rowNumber |> clamp 0 maxRowNumber)))

                SpikeFixture (Pegboard (spacing, offset, updatedPgs))
            | _ -> failwith "unexpected"
        let updatedShelf = {shelf with ProductGroups = updatedProductGroups }
        let plano = 
            planoProject.Planograms |> List.item index
            |> editShelf shelf (Some updatedShelf)
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        let updatedSelection = 
            match selection with
            | Fixture (_, searchNew, searchPg) -> updatedShelf, searchNew, searchPg
            | _ -> updatedShelf, "", ""
            |> Fixture
        (updatedPlanoProject, index, updatedSelection)

    | ToAutoLayout(newLayout, shelf) ->     
        let updatedShelf = 
            let pgs = 
                match shelf.ProductGroups with
                | Manual pgs -> pgs
                | Auto (layout, pgs) -> getXPositionedShelfProductGroups shelf.Size (layout, pgs) 
                | _ -> failwith "unexpected"
                |> List.sortBy second
                |> List.map first
            {shelf with ProductGroups = Auto(newLayout, pgs)}
        let plano = 
            planoProject.Planograms |> List.item index
            |> editShelf (shelf) (Some (updatedShelf))
        let updatedPlanoProject = 
            {planoProject with Planograms = planoProject.Planograms |> List.mapi (fun i p -> if i = index then plano else p)}
        (updatedPlanoProject, index, Fixture (updatedShelf, "", ""))
    | ChangePlanogramVariant variantIndex ->
        (planoProject, variantIndex, PlanogramVariant)
    | SelectPlanogram -> 
        (planoProject, index, PlanogramVariant)
    | SelectPlanogramProject -> 
        (planoProject, index, Project "")        
    | SearchForProductUpcOrName search ->
        match selection with
        | Project _ ->
            (planoProject, index, Project search)   
        | Fixture (s, _, searchPG) -> (planoProject, index, Fixture (s, search, searchPG))
        | _ -> (planoProject, index, selection)   
    | SearchForExistingProductGroupByUpcOrName search ->
        match selection with

        | Fixture (s, searchNew, _) -> (planoProject, index, Fixture (s, searchNew, search))
        | _ -> (planoProject, index, selection)   
    | _ -> failwith (sprintf "Unexpected message: %A" planogramMsg)

let update msg state = 
    match msg, state with
    | LoadPsa file, _ ->
        let projectOption = Parser.parsePSA file.Data       
        match projectOption with
        | Some project -> 
            Loaded(project, 0, PlanogramVariant)
        | _ -> NotLoaded
    | PlanogramMsg msg', Loaded (planoProject, index, selection) -> 
        updatePlanogram msg' (planoProject, index, selection)
        |> Loaded
    | _ -> state    
       
open React
open React.Props

//let bulmaDivider : obj = importAll "bulma-divider"  
let view state dispatch = 
    drawState state dispatch

open Elmish

Program.mkSimple init update view
|> React.Program.withReact "elmish-app"
|> Program.run 