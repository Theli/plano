module Planograms.Model
open SharedExtensions.Option
open SharedExtensions.Math

open Geometry
let random = System.Random()
let log = Fable.Import.Browser.console.log

type Color = int

type ProductInfo = 
    | ProductName of string
    | Manufacturer of string
    | Category of string
    | Supplier of string    
    | Price of float
    | Brand of string
    | SubCategory of string

type HorizontalPosition = 
    | LeftSide
    | RightSide

type VerticalPosition = 
    | Above 
    | Below 

type DepthPosition = 
    | InFront
    | Behind 

type Side = 
    | Front
    | Back
    | Left
    | Right
    | Top
    | Bottom

type Roll = 
    | Deg0
    | Deg90
    | Deg180
    | Deg270

type Orientation = 
    {Side: Side; Roll: Roll}
    override this.ToString() = 
        sprintf "%A %A" this.Side this.Roll
    static member DefaultValue = {Side = Side.Front; Roll = Deg0}

type AutofillOptionsStrict = {
    CappingOrientation: Orientation
    MaximumTotal: int
    MinimumTotal: int
    MaximumUnits: int
    MaximumCapping: int
}

type AutofillOptions = {
    MaximumTotal: int option
    MinimumTotal: int option
    MaximumUnits: int option
    MaximumCapping: int option
}

type MerchStyle =  
    | Unit = 0
    | Tray = 1
    | Case = 2

type Product = {
    Upc: string
    Size: Vector3
    NestingSize: Vector3
    SqueezeLimit: Vector3
    ExpandLimit: Vector3
    Color: Color
    ProductInfo: Set<ProductInfo>
    MainSpikeOffset: Vector2
    SecondarySpikeOffsets: Vector2 list
    AutofillOptions: AutofillOptions
}
let getProductName (product: Product) = product.ProductInfo |> Seq.tryPick (function | ProductName pn -> Some pn | _ -> None) |> Option.defaultValue "UNKNOWN"

let rotatedProductSize product orientation = 
    let roll x y z = function
        | Deg0 | Deg180 -> x, y, z
        | _ -> y, x, z        
    match orientation.Side with
    | Front | Back -> product.Size.X, product.Size.Y, product.Size.Z
    | Side.Left | Side.Right -> product.Size.Z, product.Size.Y, product.Size.X
    | Side.Top | Side.Bottom -> product.Size.X, product.Size.Z, product.Size.Y
    |||> roll <| orientation.Roll
    |||> fun x y z ->
        {X = x; Y = y; Z = z}      

type XCapping = HorizontalPosition * Orientation * int
type YCapping = VerticalPosition * Orientation * int
type ZCapping = DepthPosition * Orientation * int  

let private subgroupSize singleRotatedSize (count: int*int*int) internalSpacing = 
    let spacingSum = 
        {
            X = internalSpacing.X .* (first count - 1)
            Y = internalSpacing.Y .* (second count - 1)
            Z = internalSpacing.Z .* (third count - 1)
        }
    singleRotatedSize * count + spacingSum
let private individualProductPositions rotatedSingleSize internalSpacing count = 
    let xCount, yCount, zCount = count
    [       
        for xIndex in 0 .. (xCount - 1) do 
            for yIndex in 0 .. (yCount - 1) do
                for zIndex in 0 .. (zCount - 1) do  
                    yield 
                        {
                            X = float xIndex * (rotatedSingleSize.X + internalSpacing.X)
                            Y = float yIndex * (rotatedSingleSize.Y + internalSpacing.Y)
                            Z = -(float zIndex * (rotatedSingleSize.Z + internalSpacing.Z)) - rotatedSingleSize.Z 
                        }       
    ]

type IndividualProductView = 
    {
        LocalPosition: Vector3
        Orientation: Orientation
        RotatedSize: Vector3
    }

type ProductGroup = 
    {Product: Product; Orientation: Orientation; Count: int * int * int; XCapping: XCapping option; YCapping: YCapping option; ZCapping: ZCapping option; YAutofill: AutofillOptionsStrict; MerchStyle: MerchStyle}
    member private this.XCount = this.Count |> first
    member private this.YCount = this.Count |> second
    member private this.ZCount = this.Count |> third
    member private this.RotatedSize = rotatedProductSize this.Product 
    member this.XMainAndCappingCount = this.XCount + (this.XCapping <!> (fun (_,_,c) -> c) |> Option.defaultValue 0)
    member this.Width = 
        let rotatedX orientation = (this.RotatedSize orientation).X

        let mainWidth = 
            this.Orientation
            |> rotatedX
            |> (.*) this.XCount

        let xCappingWidth = 
            this.XCapping  
            <!> fun (_, orientation, xCappingCount) -> orientation |> rotatedX |> (.*) xCappingCount
            |> Option.defaultValue 0.

        let yCappingWidth = 
            this.YCapping  
            <!> second
            <!> rotatedX
            |> Option.defaultValue 0.

        let zCappingWidth = 
            this.ZCapping  
            <!> second
            <!> rotatedX
            |> Option.defaultValue 0.

        mainWidth + xCappingWidth
        |> max yCappingWidth
        |> max zCappingWidth

    member this.ShelfProductGroup internalSpacing autofillVerticalSpace =
        let mainSubgroupCount = 
            match autofillVerticalSpace with
            | None -> this.Count
            | Some availableHeight ->
                let maxUnits = min this.YAutofill.MaximumTotal this.YAutofill.MaximumUnits           
                let yCount = 
                    availableHeight / (this.RotatedSize this.Orientation).Y
                    |> int
                    |> max 1
                    |> fun unitsFit -> if maxUnits = 0 then unitsFit else min unitsFit maxUnits        
                (this.XCount, yCount, this.ZCount)    
              
        let mainSubgroupSize = subgroupSize (this.RotatedSize this.Orientation) mainSubgroupCount internalSpacing

        let xCappingCount = 
            this.XCapping
            >>= fun (position, orientation, xCount) ->
                let cappingRotatedSize = this.RotatedSize orientation
                let yCount = (mainSubgroupSize.Y / cappingRotatedSize.Y) |> int |> max 1
                let zCount =  (mainSubgroupSize.Z / cappingRotatedSize.Z)|> int |> max 1
                let count = (xCount, yCount, zCount)
                Some (position, orientation, count, cappingRotatedSize) 

        let yCappingCount = 
            match this.YCapping, autofillVerticalSpace with
            | Some capping, None -> 
                capping |> Some
            | _, Some availableHeight when this.YAutofill.MaximumCapping > 0 ->
                let position = this.YCapping <!> first |> Option.defaultValue VerticalPosition.Above
                let spaceLeft = availableHeight - mainSubgroupSize.Y     
                let orientation = this.YAutofill.CappingOrientation
                spaceLeft / (this.RotatedSize orientation).Y
                |> int
                |> min this.YAutofill.MaximumCapping  
                |> fun yCount -> 
                    if yCount > 0 then Some (position, orientation, yCount)
                    else None
            | _, _ -> None
            >>= (fun (position, orientation, yCount) ->
                let cappingRotatedSize = this.RotatedSize orientation
                let xCount = (this.Width / cappingRotatedSize.X) |> int |> max 1
                let zCount = (mainSubgroupSize.Z / cappingRotatedSize.Z) |> int |> max 1 
                let count = (xCount, yCount, zCount)
                Some (position, orientation, count, cappingRotatedSize) )

        let zCappingCount = 
            this.ZCapping
            >>= fun (position, orientation, zCount) ->
                let cappingRotatedSize = this.RotatedSize orientation
                let xCount = (mainSubgroupSize.X / cappingRotatedSize.X) |> int |> max 1
                let yCount = (mainSubgroupSize.Y / cappingRotatedSize.Y) |> int |> max 1
                let count = (xCount, yCount, zCount)
                Some (position, orientation, count, cappingRotatedSize)       

        let xCappingSize = xCappingCount <!> (fun (_, _, c, rotatedSize) -> internalSpacing + subgroupSize rotatedSize c internalSpacing) |> Option.defaultValue Vector3.Zero
        let yCappingSize = yCappingCount <!> (fun (_, _, c, rotatedSize) -> subgroupSize rotatedSize c internalSpacing) |> Option.defaultValue Vector3.Zero
        let zCappingSize = zCappingCount <!> (fun (_, _, c, rotatedSize) -> subgroupSize rotatedSize c internalSpacing) |> Option.defaultValue Vector3.Zero

        let leftCappingWidth = 
            xCappingCount
            >>= fun (pos, _, _, _) -> if pos = HorizontalPosition.LeftSide then Some xCappingSize.X else None
            |> Option.defaultValue 0.

        let bottomCappingHeight = 
            yCappingCount
            >>= fun (pos, _, _, _) -> if pos = VerticalPosition.Below then Some yCappingSize.Y else None
            |> Option.defaultValue 0.

        let frontCappingDepth = 
            zCappingCount
            >>= fun (pos, _, _, _) -> if pos = DepthPosition.InFront then Some zCappingSize.Z else None
            |> Option.defaultValue 0.  

        let mainSingleProducts = 
            let offset = {X = leftCappingWidth; Y = bottomCappingHeight; Z = -frontCappingDepth}
            individualProductPositions (this.RotatedSize this.Orientation) internalSpacing mainSubgroupCount
            |> List.map ((+) offset)
            |> List.map (fun pos -> {Orientation = this.Orientation; LocalPosition = pos; RotatedSize = (this.RotatedSize this.Orientation)})

        let xCappingSingleProducts =           
            match xCappingCount with
            | Some (position, orientation, count, cappingRotatedSize) -> 
                let xOffset = if position = HorizontalPosition.LeftSide then 0. else (mainSubgroupSize.X + internalSpacing.X)
                let offset = {X = xOffset; Y = bottomCappingHeight; Z = -frontCappingDepth}
                individualProductPositions cappingRotatedSize internalSpacing count
                |> List.map ((+) offset)
                |> List.map (fun pos -> {Orientation = orientation; LocalPosition = pos; RotatedSize = cappingRotatedSize})
            | _ -> []                           
            
        let yCappingSingleProducts =                          
            match yCappingCount with
            | Some (position, orientation, count, cappingRotatedSize) -> 
                let yOffset = if position = VerticalPosition.Below then 0. else mainSubgroupSize.Y
                let offset = {X = 0.; Y = yOffset; Z = -frontCappingDepth}
                individualProductPositions cappingRotatedSize internalSpacing count
                |> List.map ((+) offset)
                |> List.map (fun pos -> {Orientation = orientation; LocalPosition = pos; RotatedSize = cappingRotatedSize})
            | _ -> []                          
            
        let zCappingSingleProducts =                           
            match zCappingCount with
            | Some (position, orientation, count, cappingRotatedSize) -> 
                let zOffset = if position = DepthPosition.Behind then -mainSubgroupSize.Z else 0.
                let offset = {X = 0.; Y = 0.; Z = zOffset}
                individualProductPositions cappingRotatedSize internalSpacing count
                |> List.map ((+) offset)
                |> List.map (fun pos -> {Orientation = orientation; LocalPosition = pos; RotatedSize = cappingRotatedSize})
            | _ -> []                           
            
        let singleProducts = 
            [                            
                yield! mainSingleProducts
                yield! xCappingSingleProducts
                yield! yCappingSingleProducts
                yield! zCappingSingleProducts
            ]

        let totalSize = 
            let width = 
                mainSubgroupSize.X + xCappingSize.X
                |> max yCappingSize.X
                |> max zCappingSize.X

            let height = 
                mainSubgroupSize.Y + yCappingSize.Y
                |> max xCappingSize.Y
                |> max zCappingSize.Y
            let depth = 
                mainSubgroupSize.Z + zCappingSize.Z
                |> max xCappingSize.Z
                |> max yCappingSize.Z           
            { X = width; Y = height; Z = depth }

        singleProducts, totalSize
    member this.SpikeFixtureProductGroup spacingBetweenRows horizontalSpacing = 
        let orientation = this.Orientation
        let rotatedSingleSize = this.RotatedSize orientation
        let x = 
            match horizontalSpacing with
            | None -> 0.
            | Some x -> 
                rotatedSingleSize.X / x
                |> System.Math.Ceiling
                |> (*) x
                |> extract rotatedSingleSize.X
        let y = 
            if nearEquals spacingBetweenRows 0. then 
                0.
            else 
                rotatedSingleSize.Y / spacingBetweenRows
                |> System.Math.Ceiling
                |> (*) spacingBetweenRows
                |> extract rotatedSingleSize.Y
        let internalSpacing = {X = x; Y = y; Z = 0.}
        let singleLocalPositions = 
            individualProductPositions rotatedSingleSize internalSpacing this.Count  
            |> List.map (fun pos -> {Orientation = orientation; LocalPosition = pos; RotatedSize = rotatedSingleSize})
        let totalSize = subgroupSize rotatedSingleSize this.Count internalSpacing
        singleLocalPositions, totalSize   

type ProductGroupDecorators = 
    | Spikes of length: float * positions: Vector3 list 

type PositionedProductGroup = 
    {
        ProductGroup: ProductGroup
        LocalPosition: Vector3
        TotalSize: Vector3
        IndividualProductViews: IndividualProductView list 
        Decorators: ProductGroupDecorators option
    }
type AutoLayout = 
    | SnapLeft
    | SnapRight
    | Spaced //spacing incide pg
    | Spread

// Pegboard, Slatwall, Bar
type SpikeFixture = 
    | Pegboard of spacing: Vector2 * offset: Vector2 * (ProductGroup * (int * int)) list
    | SlatWall of spacingBetweenRows: float * offset: Vector2 * (ProductGroup * (int * float)) list
    | Bar of (ProductGroup * float) list
    member this.Offset = 
        match this with
        | Pegboard (_, offset, _) -> offset
        | SlatWall (_, offset, _) -> offset
        | Bar _ -> Vector2.Zero

    member this.SpacingBetweenRows = 
        match this with
        | Pegboard (spacing, _, _) -> spacing.Y
        | SlatWall (spacingBetweenRows, _, _) -> spacingBetweenRows   
        | Bar _ -> 0.


type ProductGroups = 
    | Manual of (ProductGroup * float) list
    | Auto of AutoLayout * ProductGroup list
    | SpikeFixture of SpikeFixture

    member this.ProductGroupList = 
        match this with
        | Manual pgs -> pgs |> List.map first
        | Auto (_, pgs) -> pgs        
        | SpikeFixture (Pegboard (_, _, pgs)) ->  pgs |> List.map first         
        | SpikeFixture (SlatWall (_, _, pgs)) ->  pgs |> List.map first   
        | SpikeFixture (Bar pgs) -> pgs |> List.map first
    override this.ToString() = 
        match this with
        | Manual _ -> "Manual"
        | Auto (layout, _) -> layout.ToString()           
        | SpikeFixture (Pegboard (spacing, offset, _)) -> sprintf "Pegboard with Spacing=%A; Offset=%A" spacing offset          
        | SpikeFixture (SlatWall (spacing, offset, _)) -> sprintf "SlatWall with spacing between rows=%f; Offset=%A" spacing offset    
        | SpikeFixture (Bar _) -> "Bar"

type Fixture = 
    {
        GeneratedId: int
        FixtureName: string
        Size: Vector3
        Color: Color
        ProductGroups: ProductGroups
        IsAutofillEnabled: bool
        SqueezeExpandEnabled: bool * bool * bool
        CanCombine: bool
    }
    member this.FixtureTypeName = 
        match this.ProductGroups with
        | Manual _ | Auto _ -> "Shelf"
        | SpikeFixture (Pegboard _) -> "Pegboard"
        | SpikeFixture (SlatWall _) -> "Slatwall"    
        | SpikeFixture (Bar _) -> "Bar"


type Planogram(planogramName: string, rawSize: Vector3, fixtures: (Fixture * Vector3) list) = //todo check when do we need raw size
    member this.PlanogramName = planogramName
    member this.Fixtures = fixtures
    member this.RawSize = rawSize


type UnitsOfMeasurement = 
    | Inches = 0
    | Centimeters = 1

type PlanogramProject = {
    ProjectName: string
    UnitsOfMeasurement: UnitsOfMeasurement
    Products: Map<string, Product>
    Planograms: Planogram list
}

type FixtureDuplicateOptions = 
    | Empty
    | WithProducts    
