module ThreeView

open Fable.Core
open Fable.Import
open Fable.Import.Three
open Fable.FileInput
open Planograms.Model
open Planograms
open Fable.Helpers
open Fable.Core.JsInterop
open Geometry
open Planograms.Calculations.FixtureLayout
open Planograms.Calculations.CanCombineShelves
open Planograms.Calculations.Planogram
let width () = Browser.window.innerWidth
let height () = Browser.window.innerHeight

let aspectRatio () = width() / height()

let initLights (scene:Three.Scene) =
    let ambientLight = Three.AmbientLight(U2.Case2 "#3C3C3C", 1.0)
    scene.add(ambientLight)
    let spotLight = Three.SpotLight(U2.Case2 "#FFFFFF")
    spotLight.position.set(-30., 60., 60.) |> ignore
    scene.add(spotLight)
    [spotLight :> Three.Light]

let initScene () =
    let scene = Three.Scene()
    scene.autoUpdate <- true
    initLights scene |> ignore
    scene

let initCamera () =
    let camera = Three.PerspectiveCamera(75.0, aspectRatio(), 0.01, 1000.0)
    camera.matrixAutoUpdate <- true
    camera.rotationAutoUpdate <- true
    camera.lookAt(Three.Vector3(0.,0.,0.)) |> ignore
    camera

let initRenderer width =
    let renderParams = createEmpty<WebGLRendererParameters>
    renderParams.antialias <- Some true
    let renderer = Three.WebGLRenderer(renderParams)
    renderer.setClearColor "#dadadb"
    (renderer :> Three.Renderer).setSize(width, width / aspectRatio())
    renderer.clear()
    renderer


let createCuboid colour {X = width; Y = height; Z = depth} =
    let geometry = BoxBufferGeometry (width, height, depth)
    geometry.translate (width/2.0, height/2.0, depth/2.0) |> ignore
    let matProps = createEmpty<MeshLambertMaterialParameters>
    matProps.color <- Some (U2.Case2 colour)
    let material = MeshLambertMaterial(matProps)
    let mesh = Mesh (geometry, material)
    mesh

open AppModel
open System
let createFixture (fixturesColor: int) (fixturesSize: Vector3) = 
    let colorString = fixturesColor |> sprintf "#%06X"
    createCuboid colorString fixturesSize

let planogramObject =

    fun (planogram: Planogram) -> 
        let root = Three.Group()
        let planoHeight = getPlanogramHeight planogram.RawSize planogram.Fixtures
        for fixture, fixturePos, positionedProductGroups in (planogram.Fixtures |> getPositionedProductGroups planoHeight) do
            let fixtureColor = 
                if (isShelfCombined fixture fixturePos planogram.Fixtures) then 0xFF0000 else fixture.Color //todo remove debug later
            let fixtureCube = createFixture fixtureColor fixture.Size
            root.add fixtureCube
            fixtureCube.position.x <- fixturePos.X
            fixtureCube.position.y <- fixturePos.Y
            fixtureCube.position.z <- fixturePos.Z

            // let autofillSpace = findAutofillSpace planogram.Shelves planogram.Size.Y shelf shelfPos 

            for i, pg in List.indexed positionedProductGroups do 
                let select () = (fixture, i) |> SelectProductGroup |> ignore //todo

                let parent = Three.Group()
                let colorString = pg.ProductGroup.Product .Color |> sprintf "#%06X"
                fixtureCube.add parent
                parent.position.x <- pg.LocalPosition.X
                parent.position.y <- pg.LocalPosition.Y
                parent.position.z <- pg.LocalPosition.Z
                for product in pg.IndividualProductViews do
                    //let rot = rollAndSideToQuaternion product.Roll product.Side
                    let productCube = createCuboid colorString product.RotatedSize
                    parent.add productCube
                    //productCube.setRotationFromQuaternion rot
                    productCube.position.x <- product.LocalPosition.X
                    productCube.position.y <- product.LocalPosition.Y
                    productCube.position.z <- product.LocalPosition.Z                
                    
        let boundingBox = Three.Box3()
        boundingBox.setFromObject(root) |> ignore
        let sphere = Three.Sphere()
        let boundingSphere: Three.Sphere = boundingBox.getBoundingSphere(sphere)
        if boundingSphere.radius <> 0. then
            let scale = 2. / boundingSphere.radius
            let scaleVector: Three.Vector3 = !!(root.scale:>Three.Vector).multiplyScalar(scale) //Mutates and returns
            let center = boundingSphere.center.multiply(scaleVector)
            root.position.set(-center.x, -center.y, -center.z) |> ignore
            root.rotation.set(0.,0.,0.) |> ignore
            root.updateMatrix()
        root

    |> Memoization.memoize    


[<RequireQualifiedAccess>]
module Spherical =
    open Math
    
    [<Literal>]
    let private MinPolarAngle = 0.0001

    [<Literal>]
    let private MaxPolarAngle = 3.141591

    let inline getRadius (spherical: Three.Spherical) = 
        let radius: float = !!spherical?radius
        radius
    
    let inline getPolarAngle (spherical: Three.Spherical) = 
        let phi: float = !!spherical?phi
        phi

    let inline getEquatorAngle (spherical: Three.Spherical) = 
        let theta: float = !!spherical?theta
        theta

    let inline setRadius radius (spherical: Three.Spherical) = 
        if radius > 0. then spherical?radius <- radius
        else spherical?radius <- 0.
        spherical
    
    let inline setPolarAngle radians (spherical: Three.Spherical) = 
        let clamped = clamp MinPolarAngle MaxPolarAngle radians
        spherical?phi <- clamped
        spherical

    let inline setEquatorAngle (radians: float) (spherical: Three.Spherical) = 
        spherical?theta <- radians
        spherical 



open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Import.Three

type private Size = {width: float; height: float}
type private Three.WebGLRenderer with
    // member this.setSize(width: float, height: float) = failwith "JS only"
    member this.setSize(width: float, height: float) = ()

type ThreeView = 
    { Camera: Three.Camera; Scene: Three.Scene; Root: Three.Group; Renderer: Three.WebGLRenderer; CameraPosition: Three.Spherical }
    member this.domElement = (this.Renderer :> Three.Renderer).domElement
    member this.render() = this.Renderer.render(this.Scene, this.Camera)

let cachedView = 
    let mutable view = None
    fun plano width -> 
        match view with
        | Some v -> 
            let size: Size = !!v.Renderer.getSize()
            if size.width <> width then v.Renderer.setSize(width, width/aspectRatio())
            let planoObj = planogramObject plano
            if planoObj = v.Root then v
            else
                v.Scene.remove v.Root
                v.Scene.add planoObj
                let updated = { v with Root = planoObj }
                view <- Some updated
                updated
            |> fun r -> 
                r.render()
                r
        | _ -> 
            let renderer = initRenderer width
            let cameraPosition = Three.Spherical(4., System.Math.PI / 2., 0.)
            let camera = initCamera ()
            camera.position.setFromSpherical cameraPosition |> ignore
            let scene = initScene () 
            let planoObj = planogramObject plano
            scene.add planoObj
            let v = {Camera = camera; Scene = scene; Root = planoObj; Renderer = renderer; CameraPosition = cameraPosition}
            view <- Some v
            v.render() 
            v
let handleMouse = 
    let zero = Three.Vector3(0., 0., 0.)
    let mutable previousMouse: (float * float) option = None
    let isPressed (e: React.MouseEvent) = 
        e.buttons <> 0. && (e.buttons |> int) % 2 = 1
    fun (view: ThreeView) (e: React.MouseEvent) ->
        match e.``type``, previousMouse, (isPressed e) with
        | ("mouseout", _, _) | ("mouseleave", _, _) -> previousMouse <- None
        | _, Some (x, y), true -> 
            let equatorDelta = (x - e.clientX) / 180.
            let polarDelta = (y - e.clientY) / 180.
            let cameraPosition = view.CameraPosition
            let polar = polarDelta + Spherical.getPolarAngle cameraPosition
            let equator = equatorDelta + Spherical.getEquatorAngle cameraPosition
            Spherical.setEquatorAngle equator cameraPosition 
            |> Spherical.setPolarAngle polar 
            //|> Spherical.setRadius radius
            |> view.Camera.position.setFromSpherical
            |> ignore
            view.Camera.lookAt zero
            view.render()
            previousMouse <- Some(e.clientX, e.clientY)
        | _, None, true -> previousMouse <- Some(e.clientX, e.clientY)
        | _ -> previousMouse <- None
        
let drawPlanogram = 
    let mutable previousView = None
    let mutable handler = ignore
    fun (dispatch: PlanogramMsg -> unit) (selection: Selection) (index: int) (planogram: Planogram) ->
        div [
            OnMouseMove (fun e -> match previousView with | Some pv -> handleMouse pv e | _ -> ())
            OnWheel(fun e -> 
                match previousView with 
                | Some pv -> 
                    let delta = if e.deltaY > 0. then 0.5 else -0.5
                    Spherical.setRadius (Spherical.getRadius pv.CameraPosition + delta) pv.CameraPosition
                    |> pv.Camera.position.setFromSpherical
                    |> ignore
                    e.preventDefault()
                    pv.render()
                | _ -> ())
        ] [
            div [
                Ref (fun e -> 
                    if e |> isNull |> not then 
                        let view = cachedView planogram e.clientWidth
                        e.appendChild(view.domElement) |> ignore
                        handler <- handleMouse view
                        previousView <- Some view
                    else previousView |> Option.iter (fun v ->  
                        let domElement =  v.domElement
                        domElement.parentElement.removeChild(domElement) |> ignore
                        previousView <- None
                        handler <- ignore))
            ] []
        ]
        
        