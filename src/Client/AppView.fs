module AppView

open Fable.Core.JsInterop
open Fable.Import
open Fable.FileInput
open Planograms.Model
open Fable.Helpers
open React
open React.Props
open Fable
open AppModel
open System
open SharedExtensions
open AppViewPrimitives
open ThreeView
open Geometry
open Planograms.Calculations.CanCombineShelves     
open Planograms.Calculations.Autofill     
open Planograms.Calculations.SpikeFixture
open Planograms.Calculations.Planogram

let private drawOrientationInput  = 
    let sides = [(Side.Front, "Front"); (Side.Back, "Back"); (Side.Left, "Left"); (Side.Right, "Right"); (Side.Top, "Top"); (Side.Bottom, "Bottom")]
    let rolls = [(Roll.Deg0, "0°"); (Roll.Deg90, "90°"); (Roll.Deg180, "180°"); (Roll.Deg270, "270°")]  

    fun (orientation: Orientation) onInput ->
        div [ClassName "field is-grouped"] [
            dropdownInput sides orientation.Side (fun selectedSide -> {Side = selectedSide; Roll = orientation.Roll} |> onInput)
            |> namedInput "Side"       
            fieldGroupSpacing
            dropdownInput rolls orientation.Roll (fun selectedRoll -> { Side = orientation.Side; Roll = selectedRoll} |> onInput)
            |> namedInput "Roll"
        ]

let private drawUnitsInput count orientation onInput = 
    let (x, y, z) = count
    div [ClassName "field"] [
        div [ClassName "field is-grouped"] [
            namedIntInput "X" 1 99 x (fun newX -> ((newX, y, z), orientation) |> onInput)
            fieldGroupSpacing
            namedIntInput "Y" 1 99 y (fun newY -> ((x, newY, z), orientation) |> onInput)
            fieldGroupSpacing
            namedIntInput "Z" 1 99 z (fun newZ -> ((x, y, newZ), orientation) |> onInput)
        ]
        drawOrientationInput orientation (fun newOrientation -> (count, newOrientation) |> onInput)
    ]
    |> labeledInput "Units"

type private Choice =
    | FirstOption
    | SecondOption

let activeLayoutButtonClass = "button is-size-7 is-info is-outlined is-selected"
let passiveLayoutButtonClass = "button is-size-7"    

let private cappingView name firstName secondName defaultChoiceAndOrientation onCappingChanged capping =
    match capping with
    | Some (amount, orientation, choice) ->
        div [ClassName "field"][
            div [ClassName "field is-grouped"] [
                yield p [ClassName "control"][
                    intInput 0 99 amount (fun c -> onCappingChanged (c, choice, orientation))
                ]
                yield div [ClassName "field"; Style [Width 115]] [
                    div [ClassName "buttons has-addons"] [
                        span [ClassName (if choice = FirstOption then activeLayoutButtonClass else passiveLayoutButtonClass); OnClick (fun _ -> onCappingChanged (amount, FirstOption, orientation))][
                            str firstName
                        ]
                        span [ClassName (if choice = FirstOption then passiveLayoutButtonClass else activeLayoutButtonClass); OnClick (fun _ -> onCappingChanged (amount, SecondOption, orientation))][
                            str secondName
                        ]
                    ]
                ]
            ]
            drawOrientationInput orientation (fun newOrientation ->  (amount, choice, newOrientation) |> onCappingChanged)
        ]
    | _ ->
        let (defaultChoice, defaultOrientation) = defaultChoiceAndOrientation
        div [ClassName "field is-grouped"] [
            p [ClassName "control"] [
                intInput 0 99 0 (fun c -> onCappingChanged (c, defaultChoice, defaultOrientation))
            ]
        ]
    |> labeledInput name

let private drawProductsTable onSearch filteredDispalyNameAndData (searchUpcOrName: string option) iconClass onClickProduct =
    let tableElements =
        filteredDispalyNameAndData
        |> Seq.map (fun (displayName, data) ->
            a [ClassName "panel-block"] [
                span [ClassName "panel-icon"] [
                    i [ClassName iconClass;  HTMLAttr.Custom ("aria-hidden", "true"); OnClick (fun _ -> onClickProduct data)] []
                ]
                span [ClassName "is-size-7"] [
                    str displayName
                ]
            ])


    nav [ClassName "panel"][
        match searchUpcOrName with
        | Some search ->
            yield div [ClassName "panel-block"] [
                p [ClassName "control has-icons-left"] [

                    input [ClassName "input is-small"; HTMLAttr.Type "text"; Placeholder "search by upc or name"; Value search; OnChange (fun ev -> ev.target?value |> string |> onSearch)]
                    span [ClassName "icon is-small is-left"] [
                        i[ClassName "fas fa-search";  HTMLAttr.Custom ("aria-hidden", "true")] []
                    ]
                ]
            ]
        | _ -> ()
        yield div [Style [MaxHeight 250; Overflow "auto"]] [
            yield! tableElements
        ]
    ]
let private drawCombinedShelfTable (selectedFixture: Fixture) (shelves: (Fixture * Vector3) list) iconClass onClickShelf  =
    let strClassName shelf = if shelf = selectedFixture then "is-size-7 has-text-weight-bold" else "is-size-7"
    let strShelfLabel shelf = 
        if shelf = selectedFixture then (sprintf "%s \"%s\" -- is selected" shelf.FixtureTypeName shelf.FixtureName) else (sprintf "%s \"%s\"" shelf.FixtureTypeName shelf.FixtureName)
    let shelfPanels =
        shelves
        |> Seq.map (fun (shelf, _) ->
            a [ClassName "panel-block"; OnClick (fun _ ->  onClickShelf shelf)] [
                span [ClassName "panel-icon"] [
                    i [ClassName iconClass;  HTMLAttr.Custom ("aria-hidden", "true")] []
                ]
                span [ClassName (strClassName shelf)] [
                    str (strShelfLabel shelf)
                ]
            ])

    nav [ClassName "panel"][
        yield div [Style [MaxHeight 250; Overflow "auto"]] [
            yield! shelfPanels
        ]
    ]
let private drawSelectionTabs dispatch index selection =
    let tabs = 
        match selection with
        | PlanogramVariant -> 
            [
                li [ClassName "is-active is-marginless"] [
                    a [] [span[] [str (sprintf "Variant №%i" (index+1))]]
                ]
                li [ClassName "is-marginless"] [
                    a [OnClick (fun _ -> SelectPlanogramProject |> dispatch)] [span[] [str "Project"]]
                ]
            ]
        | Project _ -> 
            [
                li [ClassName "is-marginless"] [
                    a [OnClick (fun _ -> SelectPlanogram|> dispatch)] [span[] [str (sprintf "Variant №%i" (index+1))]]
                ]
                li [ClassName "is-active is-marginless"] [
                    a [] [span[] [str "Project"]]
                ]
            ]
        | Fixture (fixture, _, _) ->
            [
                li [ClassName "is-active is-marginless"] [
                    a [][span[][str fixture.FixtureTypeName]]
                ]
            ]
        | ProductGroup (shelf, productGroupIndex) -> 
            [
                li [ClassName "is-active"; ] [
                    a [] [span[] [str "Position"]]
                ]
                li [ClassName "is-marginless"] [
                    a [OnClick (fun _ -> SelectProduct(shelf, productGroupIndex) |> dispatch)] [span[] [str "Product"]]
                ]
            ]
        | Product (shelf, productGroupIndex) ->
            [
                li [ClassName "is-marginless"] [
                    a [OnClick (fun _ -> SelectProductGroup(shelf, productGroupIndex) |> dispatch)][span[] [str "Position"]]
                ]
                li [ClassName "is-active is-marginless"] [
                    a [] [span[] [str "Product"]]
                ]
            ]
        | _ -> []

    div [ClassName "tabs"][
        ul [ClassName "is-marginless"] tabs
    ]

let private drawShelfLayoutInput dispatch shelf = 
    let classNameAndHandler (layoutOption): IHTMLProp list = 
        match shelf.ProductGroups, layoutOption with
        | Manual _, None -> [ClassName activeLayoutButtonClass]
        | _, None -> [ClassName passiveLayoutButtonClass;  OnClick (fun _ -> shelf |> ToManualLayout |> dispatch)]
        | Auto (autolayout, _), Some layout when autolayout = layout -> [ClassName activeLayoutButtonClass]
        | _, Some layout -> [ClassName passiveLayoutButtonClass; OnClick (fun _ ->  (layout, shelf) |> ToAutoLayout |> dispatch ) ]
        | _ -> []       

    div [ClassName "field-body"] [
        div [ClassName "buttons has-addons"] [
            span (classNameAndHandler None) [str "Manual"]
            span (classNameAndHandler (Some SnapLeft)) [str "Left"] 
            span (classNameAndHandler (Some SnapRight)) [str "Right"] 
            span (classNameAndHandler (Some Spaced)) [str "Spaced"] 
            span (classNameAndHandler (Some Spread)) [str "Spread"] 
        ]
    ]  
    |> labeledInput "Layout"
let private drawMerchStyleInput onMerchStyleInput (productGroup: ProductGroup) = 
    let classNameAndHandler merchStyle: IHTMLProp list = 
        if productGroup.MerchStyle = merchStyle then 
            [ClassName activeLayoutButtonClass] 
        else 
            [ClassName passiveLayoutButtonClass;  OnClick (fun _ -> onMerchStyleInput merchStyle)] //todo
     
    div [ClassName "field-body"] [
        div [ClassName "buttons has-addons"] [
            span (classNameAndHandler MerchStyle.Tray) [str "Tray"]
            span (classNameAndHandler MerchStyle.Case) [str "Case"]
            span (classNameAndHandler MerchStyle.Unit) [str "Unit"]            
        ]
    ]  
    |> labeledInput "Merchandising style"
    
let private drawProductGroupPositionInput dispatch shelf productGroupIndex =
    match shelf.ProductGroups with
    | Manual ppgs ->
        let xOffset = ppgs |> List.item productGroupIndex |> second
        floatInput 0. shelf.Size.X 0.05 xOffset (fun newOffset -> (MoveOnManualLayout (shelf, productGroupIndex, newOffset)) |> MoveProductGroup |> dispatch)
        |> namedInput "X"
        |> labeledInput "Horizontal offset on shelf, m"
    | Auto(_, pgs) ->
        intInput 0 ((pgs |> List.length) - 1) productGroupIndex (fun newIndex -> (MoveOnAutoLayout (shelf, productGroupIndex, newIndex)) |> MoveProductGroup |> dispatch)
        |> namedInput "№"
        |> labeledInput "Position on shelf"
    | SpikeFixture spikeFixture ->
        let lineAmount = rowCount spikeFixture.SpacingBetweenRows spikeFixture.Offset.Y shelf.Size.Y 
        match spikeFixture with 
        | Pegboard (spacing, offset, ppgs) ->
            let (xHole, yHole) = ppgs |> List.item productGroupIndex |> second
            let xHoleAmount = holesInRowCount spacing.X offset.X shelf.Size.X

            div [ClassName "field is-grouped"] [
                namedIntInput "Line" 0 (lineAmount - 1)  yHole (fun newYHole -> (MoveOnPegboard (shelf, productGroupIndex, xHole, newYHole)) |> MoveProductGroup |> dispatch) 
                fieldGroupSpacing                  
                namedIntInput "Hole number" 0 (xHoleAmount - 1)  xHole (fun newXHole -> (MoveOnPegboard (shelf, productGroupIndex, newXHole, yHole)) |> MoveProductGroup |> dispatch) 
                fieldGroupSpacing
            ]
            |> labeledInput "Position on pegboard"
        | SlatWall (_, offset, ppgs) ->
            let (lineNumber, xOffset) = ppgs |> List.item productGroupIndex |> second
            div [ClassName "field is-grouped"] [
                namedIntInput "Line" 0 (lineAmount - 1)  lineNumber (fun newLine -> (MoveOnSlatwall (shelf, productGroupIndex, newLine, xOffset)) |> MoveProductGroup |> dispatch) //todo line count
                fieldGroupSpacing
                namedFloatInput "X offset, m" 0. (shelf.Size.X - offset.X) 0.01 xOffset (fun newXOffset -> (MoveOnSlatwall (shelf, productGroupIndex, lineNumber, newXOffset)) |> MoveProductGroup |> dispatch)
            ]
            |> labeledInput "Position on slatwall"
        | Bar ppgs ->
            let xOffset = ppgs |> List.item productGroupIndex |> second
            div [ClassName "field is-grouped"] [
                // namedFloatInput "X offset, m" 0. (shelf.Size.X - xOffset) 0.01 xOffset (fun newXOffset -> (MoveOnSlatwall (shelf, productGroupIndex, lineNumber, newXOffset)) |> MoveProductGroup |> dispatch)
                namedFloatInput "X offset, m" 0. (shelf.Size.X - xOffset) 0.01 xOffset (fun newXOffset -> (MoveOnBar (shelf, productGroupIndex, newXOffset)) |> MoveProductGroup |> dispatch)
            ]
            |> labeledInput "Position on slatwall"

let private drawAutofillInput (autofillOptions: AutofillOptionsStrict) onInput =         
    div [ClassName "field"] [
        div [ClassName "field is-grouped"] [
            namedIntInput "Min Total" 1 99 (autofillOptions.MinimumTotal) (fun x -> {autofillOptions with MinimumTotal = x} |> onInput)
            fieldGroupSpacing
            namedIntInput "Max Total" 1 99 (autofillOptions.MaximumTotal) (fun x -> {autofillOptions with MaximumTotal = x} |> onInput)
        ]
        div [ClassName "field is-grouped"] [
            namedIntInput "Max Units" 0 99 (autofillOptions.MaximumUnits) (fun x -> {autofillOptions with MaximumUnits = x} |> onInput)
            fieldGroupSpacing
            namedIntInput "Max Caps" 0 99 (autofillOptions.MaximumTotal) (fun x -> {autofillOptions with MaximumCapping = x} |> onInput)
        ]
        drawOrientationInput autofillOptions.CappingOrientation ignore
    ]
    |> labeledInput "Autofill settings"

let private drawSelection dispatch (project: PlanogramProject) index selection =
    let planogram = project.Planograms |> List.item index
    let planoSize = getPlanogramSize planogram.RawSize planogram.Fixtures

    let getProductGroup shelf productGroupIndex =
        match shelf.ProductGroups with
        | Manual ppgs -> ppgs |> List.item productGroupIndex |> first
        | SpikeFixture (Pegboard (_, _, ppgs)) -> ppgs |> List.item productGroupIndex |> first
        | SpikeFixture (SlatWall (_, _, ppgs)) -> ppgs |> List.item productGroupIndex |> first
        | SpikeFixture (Bar ppgs) -> ppgs |> List.item productGroupIndex |> first
        | Auto(_, pgs) -> pgs |> List.item productGroupIndex

    match selection with
    | Nothing -> div [] []
    | PlanogramVariant ->       
        div [] [
            drawSelectionTabs dispatch index selection
            textInputReadOnly "Name" planogram.PlanogramName
            vector3Input "Size, m" planoSize ignore 
        ]
    | Project searchUpcOrName ->
        div [] [
            drawSelectionTabs dispatch index selection
            textInputReadOnly "Name" project.ProjectName
            drawProductsTable (SearchForProductUpcOrName >> dispatch) (project.Products |> Map.toSeq) (Some searchUpcOrName) "fas fa-cube" ignore
            |> labeledInput "Products"
        ]

    | Fixture (fixture, searchNewProductUpcOrName, searchExistingProductUpcOrName) ->
        let position = planogram.Fixtures |> List.find (fun (s, _) -> s = fixture) |> second
        let onXPositionChanged =  (fun x -> MoveFixture (fixture, {position with X = x}) |> dispatch)
        let onYPositionChanged =  (fun y -> MoveFixture (fixture, {position with Y = y}) |> dispatch)   
        let combinedShelves = 
            planogram.Fixtures
            |> groupCombinedShelves 
            |> List.find (fun g -> g |> List.exists (fun (s,_) -> s = fixture))

        let fixtureTypeDependentControls =
            match fixture.ProductGroups with
            | Manual _ | Auto _ ->
                [
                    yield drawShelfLayoutInput dispatch fixture

                    yield div [ClassName "field is-grouped"] [
                        namedCheckbox "X " (fixture.SqueezeExpandEnabled |> first) true ignore
                        namedCheckbox "Y " (fixture.SqueezeExpandEnabled |> second) true ignore
                        namedCheckbox "Z " (fixture.SqueezeExpandEnabled |> third) true ignore
                    ]
                    |> labeledInput "Squeeze/Expand"
                   
                    yield namedCheckbox "Y " fixture.IsAutofillEnabled false (fun isChecked -> SetShelfAutofillEnabled (fixture, isChecked) |> dispatch)                   
                    |> labeledInput "Autofill"   

                    yield namedCheckbox "Can combine  " fixture.CanCombine false (fun isChecked -> SetShelfCanCombineEnabled (fixture, isChecked) |> dispatch)                   
                    |> labeledInput "Interaction with other shelves"   

                    if combinedShelves |> List.length > 1 then
                        let shelvesWithDisplayNames = 
                            combinedShelves 
                            |> List.map (fun (s, _) ->
                                let displayName = 
                                    if fixture = s then (sprintf "%s \"%s\" -- current shelf" s.FixtureTypeName s.FixtureName) else (sprintf "%s \"%s\"" s.FixtureTypeName s.FixtureName)
                                displayName, s)
                        yield drawProductsTable ignore shelvesWithDisplayNames None "fas fa-magnet" (fun s -> if s<>fixture then s |> SelectFixture |> dispatch)
                        |> labeledInput "Is combined with"
                ]
            | SpikeFixture (Pegboard (spacing, offset, _ )) ->
                [

                    vector2Input "Holes top left offset, m" offset (fun newOffset -> EditPegboard (spacing, newOffset, fixture) |> dispatch)
                    vector2Input "Holes spacing, m" spacing (fun newSpacing -> EditPegboard (newSpacing, offset, fixture) |> dispatch)                    
                ]
            | SpikeFixture (SlatWall (ySpacing, offset, _ )) ->
                [
                    vector2Input "Rows top left offset, m" offset (fun newOffset -> EditSlatwall (ySpacing, newOffset, fixture) |> dispatch)                    
                    namedFloatInput "Y" 0.01 1. 0.01 ySpacing (fun newSpacing -> EditSlatwall (newSpacing, offset, fixture) |> dispatch)
                    |> labeledInput "Vertical spacing between rows, m"
                ]
            | _ -> 
                []

        div [] [
            yield drawSelectionTabs dispatch index selection
            yield textInput "Name" fixture.FixtureName (Some (fun n -> RenameFixture(fixture, n) |> dispatch))
            yield vector3Input "Size, m" fixture.Size (fun newSize ->  ResizeFixture (fixture, newSize) |> dispatch)

            yield div [ClassName "field is-grouped"] [
                namedFloatInput "X" -10. 10. 0.1 position.X onXPositionChanged
                fieldGroupSpacing
                namedFloatInput "Y" 0. 5. 0.1 position.Y onYPositionChanged
            ]
            |> labeledInput "Position, m"

            yield! fixtureTypeDependentControls

            let filter searchLowerString (product: Product)  =
                product.Upc.StartsWith (searchLowerString, StringComparison.OrdinalIgnoreCase)
                    || (product |> getProductName |> fun n -> n.ToLower().Contains(searchLowerString)) 


            let filteredFixtureProductGroups = 
                let isSearched = filter (searchExistingProductUpcOrName.ToLower())
                match fixture.ProductGroups with
                | Manual pgs ->
                    pgs
                    |> List.mapi (fun i (pg, xPos) -> i, pg, xPos)
                    |> List.choose (fun (i, pg, xPos) -> 
                        if isSearched pg.Product then 
                            Some ((sprintf "%s %s [x=%.2f]" pg.Product.Upc (pg.Product |> getProductName) xPos), i) 
                        else None)
                | Auto (_, pgs) ->
                    pgs
                    |> List.mapi (fun i pg -> i, pg)
                    |> List.choose (fun (i, pg) -> 
                        if isSearched pg.Product then 
                            Some ((sprintf "%s %s [index=%i]" pg.Product.Upc (pg.Product |> getProductName) i), i) 
                        else None)
                | SpikeFixture (Pegboard (_, _, pgs)) ->
                    pgs
                    |> List.mapi (fun i pg -> i, pg)
                    |> List.choose (fun (i, (pg, (column, row))) -> 
                        if isSearched pg.Product then 
                            Some ((sprintf "%s %s [row=%i; column=%i]" pg.Product.Upc (pg.Product |> getProductName) row column), i) 
                        else None)
                | SpikeFixture (SlatWall (_, _, pgs)) ->
                    pgs
                    |> List.mapi (fun i pg -> i, pg)
                    |> List.choose (fun (i, (pg, (row, xOffset))) -> 
                        if isSearched pg.Product then 
                            Some ((sprintf "%s %s [row=%i; x=%.2f]" pg.Product.Upc (pg.Product |> getProductName) row xOffset), i) 
                        else None)
                | SpikeFixture (Bar pgs) ->
                    pgs
                    |> List.mapi (fun i pg -> i, pg)
                    |> List.choose (fun (i, (pg, xOffset)) -> 
                        if isSearched pg.Product then 
                            Some ((sprintf "%s %s [x=%.2f]" pg.Product.Upc (pg.Product |> getProductName) xOffset), i) 
                        else None)

            yield drawProductsTable (SearchForExistingProductGroupByUpcOrName >> dispatch) filteredFixtureProductGroups (Some searchExistingProductUpcOrName) "fas fa-cube" (fun pgIndex -> SelectProductGroup(fixture, pgIndex) |> dispatch) 
            |> labeledInput (sprintf "%s content" fixture.FixtureTypeName) 

            let filteredPlanogramProducts = 
                let lowerSearch = searchNewProductUpcOrName.ToLower()
                project.Products 
                |> Map.toSeq
                |> Seq.filter (fun (upc: string, product) ->
                    upc.StartsWith (lowerSearch, StringComparison.OrdinalIgnoreCase)
                    || (product |> getProductName |> fun n -> n.ToLower().Contains(lowerSearch)))
                |> Seq.map (fun (upc: string, product) -> ((sprintf "%s %s" product.Upc (product |> getProductName)), product))


            yield drawProductsTable (SearchForProductUpcOrName >> dispatch) filteredPlanogramProducts (Some searchNewProductUpcOrName) "fas fa-plus has-text-success" (fun product -> CreateProductGroup(fixture, product) |> dispatch)
            |> labeledInput (sprintf "Add position to %s" (fixture.FixtureTypeName.ToLower()))

            yield br[]

            yield div [ClassName "field-body"] [
                div [ClassName "buttons has-addons"] [
                    span [ClassName passiveLayoutButtonClass;  OnClick (fun _ -> (fixture, position, WithProducts) |> DuplicateFixture |> dispatch)] [str "With products"]
                    span [ClassName passiveLayoutButtonClass;  OnClick (fun _ -> (fixture, position, FixtureDuplicateOptions.Empty) |> DuplicateFixture |> dispatch)] [str "Without products"]
                ]
            ]  
            |> labeledInput (sprintf "Duplicate %s" (fixture.FixtureTypeName.ToLower()))
            
            yield br[]

            if planogram.Fixtures |> List.length > 1 then               
                yield a [ClassName "button is-danger is-outlined is-size-7"; OnClick (fun _ -> (fixture, position) |> RemoveFixture |> dispatch )][
                    str (sprintf "Delete %s" fixture.FixtureTypeName)
                ]    

        ]
    | ProductGroup (shelf, productGroupIndex) ->
        let productGroup = getProductGroup shelf productGroupIndex

        let hostFixtureDependentControls =
            match shelf.ProductGroups with
            | Manual _ | Auto _ ->
                let onXCappingChanged = fun countChoiceOrientation ->
                    let xCap =
                        match countChoiceOrientation with
                        | (xCount, _, _) when xCount = 0 -> None
                        | (xCount, choice, orientation) when choice = FirstOption -> Some (HorizontalPosition.LeftSide, orientation, xCount)
                        | (xCount, _, orientation) -> Some (HorizontalPosition.RightSide, orientation, xCount)
                        | _ -> None
                    ModifyProductGroup({productGroup with XCapping = xCap}, shelf, productGroupIndex) |> dispatch

                let onYCappingChanged = fun countChoiceOrientation ->
                    let yCap =
                        match countChoiceOrientation with
                        | (yCount, _, _) when yCount = 0 -> None
                        | (yCount, choice, orientation) when choice = FirstOption -> Some (VerticalPosition.Above, orientation, yCount)
                        | (yCount, _, orientation) -> Some (VerticalPosition.Below, orientation,  yCount)
                        | _ -> None
                    ModifyProductGroup({productGroup with YCapping = yCap}, shelf, productGroupIndex) |> dispatch

                let onZCappingChanged = fun countChoiceOrientation ->
                    let zCap =
                        match countChoiceOrientation with
                        | (zCount, _, _) when zCount = 0 -> None
                        | (zCount, choice, orientation) when choice = FirstOption -> Some (DepthPosition.InFront, orientation, zCount)
                        | (zCount, _, orientation) -> Some (DepthPosition.Behind,orientation, zCount)
                        | _ -> None
                    ModifyProductGroup({productGroup with ZCapping = zCap}, shelf, productGroupIndex) |> dispatch
                let onMerchStyleInput = fun newStyle ->
                    ModifyProductGroup({productGroup with MerchStyle = newStyle}, shelf, productGroupIndex) |> dispatch

                [
                    productGroup.XCapping
                    |> Option.map (fun (pos, orientation, c) -> (c, orientation, (if pos = HorizontalPosition.LeftSide then FirstOption else SecondOption)))
                    |> cappingView "X Capping" "Left" "Right" (FirstOption, Orientation.DefaultValue) onXCappingChanged

                    productGroup.YCapping
                    |> Option.map (fun (pos, orientation, c) -> (c, orientation, (if pos = VerticalPosition.Above then FirstOption else SecondOption)))
                    |> cappingView "Y Capping" "Above" "Below" (FirstOption, Orientation.DefaultValue) onYCappingChanged

                    productGroup.ZCapping
                    |> Option.map (fun (pos, orientation, c) -> (c, orientation, (if pos = DepthPosition.InFront then FirstOption else SecondOption)))
                    |> cappingView "Z Capping" "InFront" "Behind" (FirstOption, Orientation.DefaultValue) onZCappingChanged

                    drawMerchStyleInput onMerchStyleInput productGroup

                    drawAutofillInput productGroup.YAutofill (fun newOptions -> ModifyProductGroup({productGroup with YAutofill=newOptions}, shelf, productGroupIndex) |> dispatch)
                ]
            | _ -> []

        div [] [
            yield drawSelectionTabs dispatch index selection
            yield textInputReadOnly "UPC" productGroup.Product.Upc
            yield textInputReadOnly "Name" (productGroup.Product |> getProductName)
            yield drawProductGroupPositionInput dispatch shelf productGroupIndex
            yield drawUnitsInput productGroup.Count productGroup.Orientation (fun (count, orientation) -> ModifyProductGroup({productGroup with Count = count; Orientation = orientation}, shelf, productGroupIndex) |> dispatch)
            yield! hostFixtureDependentControls
            yield br[]
            yield a [ClassName "button is-danger is-outlined is-size-7"; OnClick (fun _ -> (shelf, productGroupIndex) |> DeleteProductGroup |> dispatch )][
                str "Delete"
            ]
        ]
        
    | Product (shelf, productGroupIndex) ->
        let productGroup = getProductGroup shelf productGroupIndex
        let info = 
            productGroup.Product.ProductInfo
            |> Seq.map (fun i -> 
                match i with
                | ProductName s -> ("Name", s)
                | Manufacturer s -> ("Manufacturer", s)
                | Category s -> ("Category", s)
                | Supplier s -> ("Supplier", s)
                | Price p -> ("Price", sprintf "%.2f" p)
                | Brand s -> ("Brand", s)
                | SubCategory s -> ("SubCategory", s))
            |> Seq.map (fun (propName, propVal) -> textInputReadOnly propName propVal)

        div [] [
            yield drawSelectionTabs dispatch index selection
            yield textInputReadOnly "UPC" productGroup.Product.Upc
            yield! info
            yield vector3Input "Size, m" productGroup.Product.Size ignore
            yield vector3Input "Nesting size, m" productGroup.Product.NestingSize ignore
            yield vector3Input "Squeeze limit" productGroup.Product.SqueezeLimit ignore
            yield vector3Input "Expand limit" productGroup.Product.ExpandLimit ignore
            yield vector2Input "Main spike offset" productGroup.Product.MainSpikeOffset ignore
        ]

let private drawNavBar dispatch state =
    let navBarContent = 
        match state with
        | Loaded(project, selectedVariantIndex, _) ->
            let drawVariant index =
                let className = if index = selectedVariantIndex then "navbar-item is-active" else "navbar-item"
                a [ClassName className; OnClick (fun _ -> if index <> selectedVariantIndex then index |> ChangePlanogramVariant |> PlanogramMsg |> dispatch)] [
                    span [] [ str (sprintf "Variant %i" (index + 1))]
                ]
            [
                div [ClassName "navbar-start"] [
                    a [ClassName "navbar-item"; OnClick (fun _ -> SelectPlanogramProject |> PlanogramMsg |> dispatch)][
                        str project.ProjectName
                    ]
                    div [ClassName "navbar-item has-dropdown is-hoverable"][
                        a [ClassName "navbar-link"][
                            span [] [
                                str (sprintf "Variant %i" (selectedVariantIndex + 1))
                            ]
                        ]
                        div [ClassName "navbar-dropdown is-boxed"] [
                            for i in 0 .. ((project.Planograms |> List.length) - 1) do
                                yield drawVariant i
                        ]
                    ]
                ]
                div[ClassName "navbar-end"] [
                    a [ClassName "navbar-item"] [ singleFileInput [OnTextReceived (LoadPsa>>dispatch)]]
                ]
            ]               
        | _ ->
            [
                div [ClassName "navbar-start"] [
                    a [ClassName "navbar-item"] [ singleFileInput [OnTextReceived (LoadPsa>>dispatch)]]
                ] 
            ]

    nav [ClassName "navbar has-shadow"][
        div [ClassName "container"][
            div [ClassName "navbar-menu is-active"] navBarContent            
        ]
    ]

let private drawPageContent dispatch state = 
    match state with
    | Loaded(project, index, selection) ->
        let planogram = project.Planograms |> List.item index 
        // let geometryTree = Geometry.Tree<PositionedShelf>(planogram.ScreenRect, planogram.Shelves |> List.map (fun (s, pos) ->  {Shelf = s; Position = pos}) )
        groupCombinedShelves planogram.Fixtures
        |> List.sortByDescending(fun g -> g |> List.head |> second |> Geometry.Vector3.GetY)
        |> List.map (fun g -> 
            let height = g |> List.head |> second |> Geometry.Vector3.GetY
            g |> List.map (fun (s, pos) -> sprintf "[%i] %s" s.GeneratedId s.FixtureName )|> String.concat "; "
            |> sprintf "Y=%.2f: %s" height)
        |> String.concat "\n"
        |> log

        div [ClassName "columns is-active"] [
            div [ClassName "column is-two-thirds"] [
                div [Style [Overflow "auto"]] [
                    br [] 
                    SvgView.drawPlanogram (PlanogramMsg>>dispatch) selection index planogram 
                    ThreeView.drawPlanogram (PlanogramMsg>>dispatch) selection index planogram 
                ]
            ]
            div [ClassName "is-divider-vertical"] []
            div [ClassName "column is-one-third";] [
                drawSelection (PlanogramMsg>>dispatch) project index selection
            ]
        ]
    | _ -> div [][]   

let drawState state dispatch =   
    div [] [
        drawNavBar dispatch state
        div [ClassName "container"] [  
            drawPageContent dispatch state
        ]
    ] 
