
module Geometry
open System
open SharedExtensions.Option
open SharedExtensions.Math
let log = Fable.Import.Browser.console.log

type Vector2 = 
    {
        X: float
        Y: float
    }
    static member (+) (v0, v1) = 
        {X = v0.X + v1.X; Y = v0.Y + v1.Y}
    static member (-) (v0, v1) = 
        {X = v0.X - v1.X; Y = v0.Y - v1.Y}    

    static member GetX (v) = v.X
    static member GetY (v) = v.Y
    static member Zero = {X = 0.; Y = 0.}

    member this.NearEquals (that: Vector2) = 
        (nearEquals this.X that.X) && (nearEquals this.Y that.Y)

type Vector3 = 
    {
        X: float
        Y: float
        Z: float
    }
    static member (+) (v0, v1) = 
        {X = v0.X + v1.X; Y = v0.Y + v1.Y; Z = v0.Z + v1.Z}
    static member (-) (v0, v1) = 
        {X = v0.X - v1.X; Y = v0.Y - v1.Y; Z = v0.Z - v1.Z}    

    static member (*) (v, (t: int*int*int)) = 
        {X = v.X  * (t |> first |> float); Y = v.Y * (t |> second |> float); Z = v.Z * (t |> third |> float)}    
    static member GetX (v) = v.X
    static member GetY (v) = v.Y
    static member GetZ (v) = v.Z
    static member Zero = {X = 0.; Y = 0.; Z = 0.}
    static member MinValue = {X = Double.MinValue; Y = Double.MinValue; Z = Double.MinValue}
    static member MaxValue = {X = Double.MaxValue; Y = Double.MaxValue; Z = Double.MaxValue}
    static member Min first second = 
        {X = min first.X second.X; Y = min first.Y second.Y; Z = min first.Z second.Z}
    static member Max first second = 
        {X = max first.X second.X; Y = max first.Y second.Y; Z = max first.Z second.Z}

type Rectangle = 
    {
        XMin: float
        XMax: float
        YMin: float
        YMax: float        
    }
    override this.ToString() = 
        sprintf "[X (%.2f - %.2f); Y (%.2f - %.2f)]" this.XMin this.XMax this.YMin this.YMax 
    member this.Width = this.XMax - this.XMin
    member this.Height = this.YMax - this.YMin
    member this.XMid = this.XMin + this.Width / 2.
    member this.YMid = this.YMin + this.Height / 2.      

    member this.Center = {X = this.XMid; Y = this.YMid} 
    member this.TopLeft = {X = this.XMin; Y = this.YMax} 
    member this.TopRight = {X = this.XMax; Y = this.YMax} 
    member this.BottomRight = {X = this.XMax; Y = this.YMin} 
    member this.BottomLeft = {X = this.XMin; Y = this.YMin} 
    member this.Contains other = 
        this.XMin <= other.XMin
        && this.XMax >= other.XMax
        && this.YMin <= other.YMin
        && this.YMax >= other.YMax

    member this.Contains (point:Vector2) = 
        this.XMin <= point.X
        && this.XMax >= point.X
        && this.YMin <= point.Y
        && this.YMax >= point.Y       

    member this.Intersects other =
        if this.XMax < other.XMin && (notNearEquals this.XMax other.XMin) then false //this is too left
        elif this.XMin > other.XMax && (notNearEquals this.XMin other.XMax) then false //this is too right
        elif this.YMin > other.YMax && (notNearEquals this.YMin other.YMax) then false //Above
        elif this.YMax < other.YMin && (notNearEquals this.YMax other.YMin) then false //Below
        else true
    
