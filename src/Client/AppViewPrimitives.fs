module AppViewPrimitives

open Fable.Core
open Fable.Core.JsInterop
open Fable.Import
open Fable.FileInput
open Planograms.Model
open Planograms
open Fable.Helpers
let log = Browser.console.log
open React
open React.Props
open Fable
open AppModel
open System
open SharedExtensions
open Geometry

let fieldGroupSpacing = p [ClassName "control"] []

let labeledInput (name: string) x =
    div [ClassName "field"] [
        label [ClassName "label is-size-7"] [
            str name
        ]
        x
    ]
let floatInput min max step value handler =
    input [
        ClassName "input is-size-7"
        HTMLAttr.Type "number"
        Min min
        Max max
        Step step
        OnChange  (fun ev ->
            let value = ev.target?value |> string |> float
            if value >= min && value <= max then
                value |> handler)
        Value (string value)
    ]

let intInput min max value handler =
    input [
        ClassName "input is-size-7"
        HTMLAttr.Type "number"
        Pattern "[0-9]*"
        Min min
        Max max
        OnChange  (fun ev ->
            let value = ev.target?value |> string |> int
            if value >= min && value <= max then
                value |> handler)
        Value (string value)
    ]

let namedInput (name: string) input =
    div [ClassName "field has-addons is-marginless"] [
        p [ClassName "control is-marginless"] [
           a [ClassName "button is-static is-size-7"] [
               str name
           ]
        ]
        div [ClassName "control"] [
            input
        ]
    ]

let namedFloatInput (name: string) min max step value handler =
    floatInput min max step value handler
    |> namedInput name

let namedIntInput (name: string) min max value handler =
    intInput min max value handler
    |> namedInput name

let namedCheckbox (name: string) isChecked isReadOnly dispatch =
    p [ClassName "control"] [
        label [ClassName "checkbox is-size-7"][
            str name
            input [HTMLAttr.Type "checkbox"; Checked isChecked; HTMLAttr.Disabled isReadOnly; OnChange (fun _ -> if isReadOnly |> not then isChecked |> not |> dispatch)]
        ]
    ]

let vector3Input (name: string) (v: Vector3) onChange =
    div [ClassName "field is-grouped"] [
        namedFloatInput "X" 0.01 10. 0.1 v.X (fun x -> {v with X = x} |> onChange)
        fieldGroupSpacing
        namedFloatInput "Y" 0.01 10. 0.1 v.Y (fun y -> {v with Y = y} |> onChange)
        fieldGroupSpacing
        namedFloatInput "Z" 0.01 10. 0.1 v.Z (fun z -> {v with Z = z} |> onChange)
    ]
    |> labeledInput name

let vector2Input (name: string) (v: Vector2) onChange =
    div [ClassName "field is-grouped"] [
        namedFloatInput "X" 0.01 10. 0.01 v.X (fun x -> {v with X = x} |> onChange)
        fieldGroupSpacing
        namedFloatInput "Y" 0.01 10. 0.01 v.Y (fun y -> {v with Y = y} |> onChange)
    ]
    |> labeledInput name


let textInput (name: string) x onInputOption =
    div [ClassName "field"][
        label [ClassName "label is-size-7"][
            str name
        ]
        div [ClassName "control"][
            input [
                ClassName "input is-size-7";
                HTMLAttr.Type "text";
                Value x;
                HTMLAttr.ReadOnly (onInputOption |> Option.isNone);
                OnChange (fun ev -> if onInputOption.IsSome then ev.target?value |> string |> onInputOption.Value ) ]
        ]
    ]

let textInputReadOnly (name: string) x  =
    textInput name x None

let dropdownInput<'t when 't: equality> (pairs: ('t * string) list) (selectedValue: 't) (onSelect: 't -> unit) = 
    let randomId = Guid.NewGuid().ToString() 
    let handler = fun _ ->
        let userChoice = (Browser.document.getElementById randomId)?value |> string
        pairs 
        |> List.pick (fun (v, label) -> if  label = userChoice then Some v else None)   
        |> onSelect
    let selectedDisplayName = pairs |> List.find (fun (value, _) -> selectedValue = value) |> second

    div [ClassName "field"][
        div [ClassName "control"] [
            span [ClassName "select is-size-7"][
                select
                    [
                        Id randomId;
                        OnChange handler
                        Value selectedDisplayName
                    ]
                    [
                        for (_, displayName) in pairs -> option [] [str displayName]
                    ]
                ]
        ]
    ]
